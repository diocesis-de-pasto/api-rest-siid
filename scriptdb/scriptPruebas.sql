insert into modules values (default,'Usuario','sub',0,'user/principal','people','U','Módulo usuario padre',null);
insert into modules values (default,'Crear usuario','child',1,'user/create',null,'CU','Módulo crear usuario','C');
insert into modules values (default,'Editar usuario','child',1,'user/edit/',null,'EU','Módulo editar usuario','U');
insert into modules values (default,'Listar usuario','child',1,'user/view/',null,'LU','Módulo listar usuario','R');
insert into modules values (default,'Eliminar usuario','child',1,null,null,'DU','Módulo eliminar usuario','D');

insert into modules values (default,'Datos personales','sub',0,null,null,'P','Módulo datos personales',null);
insert into modules values (default,'Listar datos personales','child',6,null,null,'PL','Módulo listar datos personales','R');
insert into modules values (default,'Crear datos personales','child',6,null,null,'PC','Módulo crear datos personales','C');
insert into modules values (default,'Editar datos personales','child',6,null,null,'PU','Módulo editar datos personales','U');
insert into modules values (default,'Eliminar datos personales','child',6,null,null,'PD','Módulo eliminar datos personales','D');

insert into modules values (default,'Administración de roles','sub',0,'role/principal','post_add','R','Módulo administración de roles',null);
insert into modules values (default,'Listar roles','child',11,'role/view/',null,'RL','Módulo listar roles','R');
insert into modules values (default,'Crear rol','child',11,'role/create',null,'RC','Módulo crear roles','C');
insert into modules values (default,'Editar rol','child',11,'role/edit/',null,'RU','Módulo editar roles','U');
insert into modules values (default,'Eliminar rol','child',11,null,null,'RD','Módulo eliminar roles','D');

insert into modules values (default,'Administración de módulos','sub',0,'module/principal','view_module','M','Módulo administración de módulos',null);
insert into modules values (default,'Listar módulos','child',16,'module/view/',null,'ML','Módulo listar módulos de la app','R');
insert into modules values (default,'Crear módulo','child',16,'module/create',null,'MC','Módulo crear módulos de la app','C');
insert into modules values (default,'Editar módulo','child',16,'module/edit/',null,'MU','Módulo editar módulos de la app','U');
insert into modules values (default,'Eliminar módulo','child',16,null,null,'MD','Módulo eliminar módulos de la app','D');

insert into modules values (default,'Administración información ministerial','sub',0,null,null,'MN','Módulo administración de información ministerial',null);
insert into modules values (default,'Listar informacion ministerial','child',21,null,null,'MNL','Módulo listar información ministerial','R');
insert into modules values (default,'Crear informacion ministerial','child',21,null,null,'MNC','Módulo crear información ministerial','C');
insert into modules values (default,'Editar informacion ministerial','child',21,null,null,'MNU','Módulo editar información ministerial','U');
insert into modules values (default,'Eliminar informacion ministerial','child',21,null,null,'MND','Módulo eliminar información ministerial','D');

insert into modules values (default,'Administración instituciones','sub',0,null,null,'I','Módulo administración de instituciones',null);
insert into modules values (default,'Listar institución','child',26,null,null,'IL','Módulo listar institución','R');
insert into modules values (default,'Crear institución','child',26,null,null,'IC','Módulo crear institución','C');
insert into modules values (default,'Editar institución','child',26,null,null,'IU','Módulo editar institución','U');
insert into modules values (default,'Eliminar institución','child',26,null,null,'ID','Módulo eliminar institución','D');

insert into modules values (default,'Administración contactos familiares','sub',0,null,null,'F','Módulo administración contactos familiares',null);
insert into modules values (default,'Listar contactos familiares','child',31,null,null,'FL','Módulo listar contactos familiares','R');
insert into modules values (default,'Crear contacto familiar','child',31,null,null,'FC','Módulo crear contactos familiares','C');
insert into modules values (default,'Editar contacto familiar','child',31,null,null,'FU','Módulo editar contactos familiares','U');
insert into modules values (default,'Eliminar contacto familiar','child',31,null,null,'FD','Módulo eliminar contactos familiares','D');

insert into modules values (default,'Administración permisos','sub',0,'permissions','vpn_key','PR','Módulo administración de permisos',null);
insert into modules values (default,'Listar permisos','child',36,null,null,'PRL','Módulo listar permisos','R');
insert into modules values (default,'Editar permiso','child',36,null,null,'PRU','Módulo editar permiso','U');

insert into modules values (default,'Administración qr','sub',0,null,null,'Q','Módulo administración de código QR',null);
insert into modules values (default,'Obtener código qr','child',39,null,'','QL','Módulo listar código QR','R');

insert into modules values (default,'Administración de reconocimientos','sub',0,null,null,'R','Módulo administración de reconocimientos',null);
insert into modules values (default,'Listar reconocimientos','child',41,null,null,'RL','Módulo listar reconocimientos','R');
insert into modules values (default,'Crear reconocimiento','child',41,null,null,'RC','Módulo crear reconocimiento','C');
insert into modules values (default,'Editar reconocimiento','child',41,null,null,'RU','Módulo editar reconocimiento','U');
insert into modules values (default,'Eliminar reconocimiento','child',41,null,null,'RD','Módulo eliminar reconocimiento','D');

insert into modules values (default,'Administración de datos académicos','sub',0,null,null,'AC','Módulo administración de datos académicos',null);
insert into modules values (default,'Listar datos académicos','child',46,null,null,'ACL','Módulo listar datos académicos','R');
insert into modules values (default,'Crear datos académicos','child',46,null,null,'ACC','Módulo crear datos académicos','C');
insert into modules values (default,'Editar datos académicos','child',46,null,null,'ACU','Módulo editar datos académicos','U');
insert into modules values (default,'Eliminar datos académicos','child',46,null,null,'ACD','Módulo eliminar datos académicos','D');

insert into modules values (default,'Administración de tipos de usuario','sub',0,null,null,'T','Módulo administración de tipos de usuarios',null);
insert into modules values (default,'Listar tipo de usuario','child',51,null,null,'TL','Módulo listar tipos de usuario','R');
insert into modules values (default,'Crear tipo de usuario','child',51,null,null,'TC','Módulo crear tipo de usuario','C');
insert into modules values (default,'Editar tipo de usuario','child',51,null,null,'TU','Módulo editar tipo de usuario','U');
insert into modules values (default,'Eliminar tipo de usuario','child',51,null,null,'TD','Módulo eliminar tipo de usuario','D');

insert into modules values (default,'Administración de datos pastorales','sub',0,null,null,'DP','Módulo administración de datos pastorales',null);
insert into modules values (default,'Listar datos pastorales','child',56,null,null,'DPL','Módulo listar datos pastorales','R');
insert into modules values (default,'Crear datos pastorales','child',56,null,null,'DPC','Módulo crear datos pastorales','C');
insert into modules values (default,'Editar datos pastorales','child',56,null,null,'DPU','Módulo editar datos pastorales','U');
insert into modules values (default,'Eliminar datos pastorales','child',56,null,null,'DPD','Módulo eliminar datos pastorales','D');

insert into modules values (default,'Administración de carnetización','sub',0,null,null,'CNT','Módulo administrar carnetización',null);
insert into modules values (default,'Crear carnet','child',61,null,null,'CNL','Módulo listar carnet','C');

insert into modules values (default,'Estadística','sub',0,null,null,'ET','Módulo estadísticas',null);
insert into modules values (default,'Listar estadísticas','child',63,null,null,'ETL','Módulo listar estadísticas','R');

insert into institutions values (default,'Diócesis de Pasto','ddp','Sacerdotes diócesis');
insert into institutions values (default,'Seminario Mayor','sm','Institución seminario');

insert into types_users values (default,'sac','Sacerdote','Sacerdote Diócesis de Pasto','{"informacionPersonal": true,"informacionMinisterial": true,"informacionAcademica": true,"experienciaPastoral": true,"reconocimientos": true,"contactoFamiliar": true}');
insert into types_users values (default,'secsem','Secretaria','Secretaria seminario','{"informacionPersonal": true,"informacionMinisterial": false,"informacionAcademica": false,"experienciaPastoral": false,"reconocimientos": true,"contactoFamiliar": true}');
insert into types_users values (default,'empsem','Empleado','Empleado seminario','{"informacionPersonal": true,"informacionMinisterial": false,"informacionAcademica": true,"experienciaPastoral": false,"reconocimientos": true,"contactoFamiliar": true}');
insert into types_users values (default,'sem','Seminarista','Seminarista','{"informacionPersonal": true,"informacionMinisterial": false,"informacionAcademica": true,"experienciaPastoral": false,"reconocimientos": false,"contactoFamiliar": true}');
insert into types_users values (default,'empdio','Empleado','Empleado Diócesis de Pasto','{"informacionPersonal": true,"informacionMinisterial": false,"informacionAcademica": true,"experienciaPastoral": false,"reconocimientos": true,"contactoFamiliar": true}');

insert into data_person values (default,Date('2008/12/31'),'Pasto','Juan','Pablo','Tumal','Rosero',Date('2008/12/31'),'Pasto','Nariño','Colombia','M','photo','Calle 10','C++','3101232345',null);

insert into ministerial_information values (default,Date('2008/12/31'),Date('2008/12/31'),null);

insert into family_contact values (default,'Carlos David Tumal P','3001471234',null,null,'Padre');

insert into roles values (default,'root','Permite cualquier cambio');
insert into roles values (default,'sacerdote','Permite cambios en su entidad');

insert into permissions values (default,true,1,1);
insert into permissions values (default,true,1,2);
insert into permissions values (default,true,1,3);
insert into permissions values (default,true,1,4);
insert into permissions values (default,true,1,5);
insert into permissions values (default,true,1,6);
insert into permissions values (default,true,1,7);
insert into permissions values (default,true,1,8);
insert into permissions values (default,true,1,9);
insert into permissions values (default,true,1,10);
insert into permissions values (default,true,1,11);
insert into permissions values (default,true,1,12);
insert into permissions values (default,true,1,13);
insert into permissions values (default,true,1,14);
insert into permissions values (default,true,1,15);
insert into permissions values (default,true,1,16);
insert into permissions values (default,true,1,17);
insert into permissions values (default,true,1,18);
insert into permissions values (default,true,1,19);
insert into permissions values (default,true,1,20);
insert into permissions values (default,true,1,21);
insert into permissions values (default,true,1,22);
insert into permissions values (default,true,1,23);
insert into permissions values (default,true,1,24);
insert into permissions values (default,true,1,25);
insert into permissions values (default,true,1,26);
insert into permissions values (default,true,1,27);
insert into permissions values (default,true,1,28);
insert into permissions values (default,true,1,29);
insert into permissions values (default,true,1,30);
insert into permissions values (default,true,1,31);
insert into permissions values (default,true,1,32);
insert into permissions values (default,true,1,33);
insert into permissions values (default,true,1,34);
insert into permissions values (default,true,1,35);
insert into permissions values (default,true,1,36);
insert into permissions values (default,true,1,37);
insert into permissions values (default,true,1,38);
insert into permissions values (default,true,1,39);
insert into permissions values (default,true,1,40);
insert into permissions values (default,true,1,41);
insert into permissions values (default,true,1,42);
insert into permissions values (default,true,1,43);
insert into permissions values (default,true,1,44);
insert into permissions values (default,true,1,45);
insert into permissions values (default,true,1,46);
insert into permissions values (default,true,1,47);
insert into permissions values (default,true,1,48);
insert into permissions values (default,true,1,49);
insert into permissions values (default,true,1,50);
insert into permissions values (default,true,1,51);
insert into permissions values (default,true,1,52);
insert into permissions values (default,true,1,53);
insert into permissions values (default,true,1,54);
insert into permissions values (default,true,1,55);
insert into permissions values (default,true,1,56);
insert into permissions values (default,true,1,57);
insert into permissions values (default,true,1,58);
insert into permissions values (default,true,1,59);
insert into permissions values (default,true,1,60);
insert into permissions values (default,true,1,61);
insert into permissions values (default,true,1,62);
insert into permissions values (default,true,1,63);

insert into permissions values (default,false,2,1);
insert into permissions values (default,false,2,3);
insert into permissions values (default,false,2,4);
insert into permissions values (default,false,2,6);
insert into permissions values (default,false,2,7);
insert into permissions values (default,false,2,8);
insert into permissions values (default,false,2,9);
insert into permissions values (default,false,2,21);
insert into permissions values (default,false,2,22);
insert into permissions values (default,false,2,23);
insert into permissions values (default,false,2,24);
insert into permissions values (default,false,2,31);
insert into permissions values (default,false,2,32);
insert into permissions values (default,false,2,33);
insert into permissions values (default,false,2,34);
insert into permissions values (default,false,2,39);
insert into permissions values (default,false,2,40);
insert into permissions values (default,false,2,41);
insert into permissions values (default,false,2,42);
insert into permissions values (default,false,2,43);
insert into permissions values (default,false,2,44);
insert into permissions values (default,false,2,46);
insert into permissions values (default,false,2,47);
insert into permissions values (default,false,2,48);
insert into permissions values (default,false,2,49);
insert into permissions values (default,false,2,56);
insert into permissions values (default,false,2,57);
insert into permissions values (default,false,2,58);
insert into permissions values (default,false,2,59);

insert into users values (default,'1080333333','C.C.','hugoandres272@gmail.com',null,'$2a$10$/jWkBHayRj5ePxs6.1kJ2OZUStPTzrQ/awbL6Zu78dC5M4IxUOSEy','enable','e02b2e15-d338-4b0b-963e-6fb16913c73a',1,1,1,null,null,null,1);
insert into users values (default,'4124124124','C.C.','dzambrano863@gmail.com',null,'$2a$10$AGIl5G9Z2fHDaouQlKVd1eAV8OeEFTWycPp22TFASb6mtBTINjpJy','enable','a170552d-8ccb-4661-a7ed-8729b7691ba0',1,1,1,null,null,null,1);
insert into users values (default,'1085309112','C.C.','barcoivonnec@gmail.com',null,'$2a$10$qlqIYQtDcvx/1zz72mPE.eFaD4DSTWB2ZK3nfpDpWeM36bvtWO9MW','enable','d3a3286e-cfb6-4e2a-8405-229b3797bae5',1,1,1,null,null,null,1);

insert into data_academy values (default,'Bachiller','Inem','Bachiller académio','2000','2000',Date('2000/12/10'),Date('2000/12/10'),'Graduado','80','Pasto',3);

insert into data_pastoral values (default,'Párroco','San Juan Bautista - Pasto',Date('2015/02/12'),'28342',Date('2015/02/12'),Date('2015/02/20'),Date('2016/02/12'),3);

insert into recognition values (default,'Servicios a la comunidad',Date('2018/10/15'),'Gobernación de Nariño',3);
