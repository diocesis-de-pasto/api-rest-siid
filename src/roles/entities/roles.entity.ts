import { PermissionEntity } from "src/permission/entities/permission.entity";
import { UserEntity } from "src/user/entities/user.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('roles')
export class RolesEntity {
    @PrimaryGeneratedColumn()
    idRole:number;

    @Column()
    nameRole: string;

    @Column()
    description: string;

    @OneToMany(() => UserEntity, (user) => user.role, {
        nullable: true
    })
    userRol: UserEntity[];

    @OneToMany(()=>PermissionEntity,(per)=>per.roleIdPermission, {
        onDelete: 'CASCADE'
    })
    permission: PermissionEntity[];
}