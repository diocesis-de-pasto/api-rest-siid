import { Module } from '@nestjs/common';
import { RolesService } from './services/roles.service';
import { RolesController } from './controllers/roles.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesEntity } from './entities/roles.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RolesEntity,
      UserEntity, 
      AuditHandlerEntity, 
      LogExceptionHandlerEntity,
    ]),
  ],
  providers: [
    RolesService,
    AuditHandlerService, 
    LogExceptionHandlerService,
  ],
  controllers: [RolesController]
})
export class RolesModule {}
