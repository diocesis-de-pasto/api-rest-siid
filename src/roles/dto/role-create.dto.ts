import {
    IsString,
    IsNotEmpty,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from 'src/user/entities/user.entity';
import { PermissionEntity } from 'src/permission/entities/permission.entity';

export class RoleCreateDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    nameRole: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    description: string;

    @IsNotEmpty()
    @ApiProperty()
    userRol: UserEntity[];

    @IsNotEmpty()
    @ApiProperty()
    permission: PermissionEntity[];
}

export class UpdateRoleDto extends PartialType(RoleCreateDto) {}