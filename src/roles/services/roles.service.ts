import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoleCreateDto, UpdateRoleDto } from '../dto/role-create.dto';
import { RolesEntity } from '../entities/roles.entity';

@Injectable()
export class RolesService {
    constructor(
        @InjectRepository(RolesEntity)
        private readonly rolesRepository: Repository<RolesEntity>,
    ) { }

    async getOne(idRole: number) {
        const role = await this.rolesRepository.findOne( idRole );
        return role;
    }

    async getAll() {
        const roles = await this.rolesRepository.find();
        return roles;
    }

    async findByName(nameRole: string) {
        return await this.rolesRepository.findOne({ nameRole });
    }

    async create(data: RoleCreateDto) {
        return await this.rolesRepository.save(data);
    }

    async update(idRole: number, data: UpdateRoleDto) {
        var updateRole = await this.rolesRepository.findOne(idRole);
        this.rolesRepository.merge(updateRole, data);
        return await this.rolesRepository.save(updateRole);
    }

    async delete(idRole: number) {
        return await this.rolesRepository.delete(idRole);
    }
}
