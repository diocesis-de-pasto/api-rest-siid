import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { RoleCreateDto, UpdateRoleDto } from '../dto/role-create.dto';
import { RolesService } from '../services/roles.service';

@ApiTags('roles')
@Controller('roles')
export class RolesController {
    constructor(private readonly rolesService: RolesService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar roles')
    @Get(':roleId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('roleId', ParseIntPipe) roleId: number) {
        const rol = await this.rolesService.getOne(roleId)
        .catch(err=>this.logExcHandler.create(req, err));
        if (rol) {
            return rol;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los rol no se ha podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar roles')
    @Get()
    @ApiBearerAuth('Authorization')
    async getAll(@Req() req) {
        const roles = await this.rolesService.getAll()
        .catch(err=>this.logExcHandler.create(req, err));
        if (roles) {
            return roles;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los roles no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear rol')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: RoleCreateDto) {
        const role = await this.rolesService.findByName(data.nameRole)
        .catch(err=>this.logExcHandler.create(req, err));
        if (!role) {
            const newRol = await this.rolesService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo crear el rol',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'RolesEntity', JSON.stringify(null), JSON.stringify(newRol));
            return new HttpException('El rol se creó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El rol ya está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar rol')
    @Put(':idRole')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateRoleDto, @Param('idRole') idRole: number) {
        const role = await this.rolesService.getOne(idRole)
        .catch(err=>this.logExcHandler.create(req, err));
        if (role) {
            const roleUpdate = await this.rolesService.update(role.idRole, data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo actualizar el rol',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'RolesEntity', JSON.stringify(role), JSON.stringify(roleUpdate));
            return new HttpException('El rol se actualizó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El rol no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar rol')
    @Delete(':idRole')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idRole') idRole: number) {
        const role = await this.rolesService.getOne(idRole)
        .catch(err=>this.logExcHandler.create(req, err));
        if (role) {
            const roleDelete = await this.rolesService.delete(idRole)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo eliminar el rol',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'RolesEntity', JSON.stringify(role), JSON.stringify(null));
            return new HttpException('El rol se eliminó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El rol no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
