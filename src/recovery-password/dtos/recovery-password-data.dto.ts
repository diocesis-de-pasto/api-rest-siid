import {
    IsNotEmpty,
    IsEmail,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PasswordRecoveryDataDto {
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty()
    readonly password: string;
}