import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/services/auth.service';
import { InstitutionEntity } from 'src/institution/entities/institution.entity';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { QrService } from 'src/qr/services/qr.service';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { TypesUserService } from 'src/types-user/services/types-user.service';
import { DataPersonEntity } from 'src/user/entities/data-person.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';
import { RecoveryPasswordController } from './controllers/recovery-password.controller';
import { RecoveryPasswordService } from './services/recovery-password.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity, 
      InstitutionEntity,
      TypesUserEntity,
      DataPersonEntity,
      LogExceptionHandlerEntity,
      TypesUserEntity
    ]),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        secret: config.get<string>('SECRET_JWT_RECOVERY'),
        signOptions: { expiresIn: '600s' },
      })
    })
  ],
  controllers: [RecoveryPasswordController],
  providers: [
    RecoveryPasswordService,
    UserService,
    AuthService,
    QrService,
    LogExceptionHandlerService,
    TypesUserService
  ]
})
export class RecoveryPasswordModule {}
