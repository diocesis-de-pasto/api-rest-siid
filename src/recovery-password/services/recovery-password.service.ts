import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { hash } from 'bcryptjs';
import { SendEmail } from 'src/email/sendEmail';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RecoveryPasswordService {
    constructor(
        private readonly jwtService: JwtService,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
    ){}

    async generateRecovery(user:UserEntity){
        const json = this.jwtService.sign({
            idUser: user.idUser
        })
        const jsonData = {
            email: user.email,
            token: Buffer.from(JSON.stringify(json)).toString('base64')
        }
        SendEmail('RestorePassword',jsonData);
        return true;
    }

    async verifyToken(token: string) {
        const jwt = Buffer.from(token,'base64').toString('ascii');
        var req = null;
        try {
            req = this.jwtService.verify(jwt.substring(1,jwt.length-1));
        } catch (error) {
            return false;
        }
        if(req!==null){
            const {password,...user} = await this.userRepository.findOne(req.idUser);
            return user;
        }
    }

    async updateUserPassword(idUser:number,password:string) {
        const user = await this.userRepository.findOne(idUser);
        user.password = await hash(password,10);
        return await this.userRepository.save(user);
    }
}
