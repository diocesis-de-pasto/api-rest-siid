import { Body, Controller, Get, HttpException, HttpStatus, Param, Post, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserService } from 'src/user/services/user.service';
import { PasswordRecoveryDataDto } from '../dtos/recovery-password-data.dto';
import { PasswordRecoveryDto } from '../dtos/recovery-password.dto';
import { RecoveryPasswordService } from '../services/recovery-password.service';

@ApiTags('recovery-password')
@Controller('recovery-password')
export class RecoveryPasswordController {
    constructor(
        private readonly userService: UserService,
        private readonly recoveryService: RecoveryPasswordService,
    ){}

    @Get('validationToken/:token')
    async validationTokenPassword(@Param('token') token:string) {
        const tk = await this.recoveryService.verifyToken(token);   
        if(tk){
                return new HttpException('Token correcto', HttpStatus.OK);
        }else{
            throw new HttpException({
                status: HttpStatus.UNAUTHORIZED,
                error: 'Token inválido',
            }, HttpStatus.UNAUTHORIZED);
        }
    }

    @Post(':token')
    async changePassword(@Req() req,@Param('token') token:string,@Body() data:PasswordRecoveryDataDto) {
        const tk = await this.recoveryService.verifyToken(token)
        if(tk && data.password!==undefined && data.password!==''){
            const updateUser = await this.recoveryService.updateUserPassword(tk.idUser,data.password)
            if(updateUser){
                return new HttpException('La contraseña se actualizó correctamente', HttpStatus.OK);
            }else{
                throw new HttpException({
                    status: HttpStatus.CONFLICT,
                    error: 'No se pudo actualizar la contraseña',
                }, HttpStatus.CONFLICT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.UNAUTHORIZED,
                error: 'Token inválido o contraseña inválida',
            }, HttpStatus.UNAUTHORIZED);
        }
    }

    @Post()
    async generateRecovery(@Body() data:PasswordRecoveryDto) {
        const userFind = await this.userService.findOneEmail(data.email)
        if(userFind){
            await this.recoveryService.generateRecovery(userFind);
            return new HttpException('Verifique su correo y siga las instrucciones', HttpStatus.OK);
        }else{
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Usuario no encontrado',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
