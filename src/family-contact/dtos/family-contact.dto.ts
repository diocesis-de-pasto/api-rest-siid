import {
    IsNotEmpty,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from 'src/user/entities/user.entity';

export class FamilyContactCreateDto {
    @IsNotEmpty()
    @ApiProperty()
    fullName: string;

    @IsNotEmpty()
    @ApiProperty()
    phoneOne: string;

    @ApiProperty()
    phoneTwo: string;

    @ApiProperty()
    phoneThree: string;

    @ApiProperty()
    relationship: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly user: UserEntity;
}

export class UpdateFamilyContactDto extends PartialType(FamilyContactCreateDto) {}