import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "src/user/entities/user.entity";

@Entity('family_contact')
export class FamilyContactEntity {
  @PrimaryGeneratedColumn()
  idFamilyContact: number;

  @Column()
  fullName: string;

  @Column()
  phoneOne: string;

  @Column({ nullable: true })
  phoneTwo: string;

  @Column({ nullable: true })
  phoneThree: string;

  @Column()
  relationship: string;

  @OneToOne(() => UserEntity, (user) => user.familyContact)
  user: UserEntity;
}
