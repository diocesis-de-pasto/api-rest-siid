import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';
import { FamilyContactCreateDto, UpdateFamilyContactDto } from '../dtos/family-contact.dto';
import { FamilyContactService } from '../services/family-contact.service';

@ApiTags('family-contact')
@Controller('family-contact')
export class FamilyContactController {
    constructor(private readonly familyContactServices: FamilyContactService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService,
        private readonly userService: UserService,
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar contactos familiares')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const familyContact = await this.familyContactServices.getFamilyContact(user)
            .catch(err => this.logExcHandler.create(req, err));
        if (familyContact) {
            if(req.user.idUser!==userId){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return familyContact;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El contacto del usuario no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear contacto familiar')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: FamilyContactCreateDto) {
        const user = new UserEntity();
        user.idUser = data.user.idUser;
        const familyContactFind = await this.familyContactServices.getFamilyContact(user)
            .catch(err=>this.logExcHandler.create(req, err));
        if (!familyContactFind) {
            if(req.user.idUser!==data.user.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const familyContactNew = await this.familyContactServices.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo guardar el contacto del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'FamilyContactEntity', JSON.stringify(null), JSON.stringify(familyContactNew));
            return new HttpException('El contacto del usuario se guardó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El contacto del usuario ya se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar contacto familiar')
    @Put(':idFamilyContact')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateFamilyContactDto, @Param('idFamilyContact', ParseIntPipe) idFamilyContact: number) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const familyContact = await this.familyContactServices.getFamilyContact(user)
                .catch(err => this.logExcHandler.create(req, err));                
            if (familyContact) {
                const familyContactUpdate = await this.familyContactServices.update(idFamilyContact, data)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo actualizar el contacto del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'FamilyContactEntity', JSON.stringify(familyContact), JSON.stringify(familyContactUpdate));
                return new HttpException('El contacto se actualizó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no ha creado los datos',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar contacto familiar')
    @Delete()
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Body() data: FamilyContactCreateDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            if (req.user.idUser !== userFind.idUser) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            const familyContact = await this.familyContactServices.getFamilyContact(user)
                .catch(err => this.logExcHandler.create(req, err))
            if (familyContact) {
                const familyContactDelete = await this.familyContactServices.delete(familyContact.idFamilyContact)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar el contacto del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'FamilyContactEntity', JSON.stringify(familyContact), JSON.stringify(null));
                return new HttpException('El contacto familiar del usuario se eliminó correctamente', HttpStatus.OK);

            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no tiene contactos guardados',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
