import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { FamilyContactCreateDto, UpdateFamilyContactDto } from '../dtos/family-contact.dto';
import { FamilyContactEntity } from '../entities/family-contact.entity';

@Injectable()
export class FamilyContactService {
    constructor(
        @InjectRepository(FamilyContactEntity)
        private readonly familyContactRepository: Repository<FamilyContactEntity>,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>
    ) { }

    async getFamilyContact(userEntity: UserEntity) {
        const {user,...familyContact} = await this.familyContactRepository.findOne({ user: userEntity },{
            relations: ["user"]
        });
        return familyContact;
    }

    async create(data: FamilyContactCreateDto) {
        const familyContactNew = await this.familyContactRepository.create(data);
        const familyContactSave = await this.familyContactRepository.save(familyContactNew);
        return familyContactSave;
    }

    async update(idFamilyContact: number, dataUpdate: UpdateFamilyContactDto) {
        const familyContactUpdate = await this.familyContactRepository.findOne(idFamilyContact);
        this.familyContactRepository.merge(familyContactUpdate, dataUpdate);
        const { user, ...recognitionReady } = familyContactUpdate;
        return await this.familyContactRepository.save(recognitionReady);
    }

    async delete(idFamilyContact: number) {
        const familyContactDelete = await this.familyContactRepository.findOne(idFamilyContact,{
            relations: ['user']
        });
        const user = await this.userRepository.findOne(familyContactDelete.user.idUser)        
        user.familyContact = null;
        await this.userRepository.save(user);
        return await this.familyContactRepository.delete(idFamilyContact);
    }
}
