import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class TypeUserDto {
    @IsNumber()
    @ApiProperty()
    idTypesUser:number;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    abbreviationTypeUser:string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    nameTypeUser:string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    descriptionTypeUser:string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    permissionsProfileUser:string;
}

export class UpdateTypeUserDto extends PartialType(TypeUserDto) {}
