import { UserEntity } from "src/user/entities/user.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('types_users')
export class TypesUserEntity {
    @PrimaryGeneratedColumn()
    idTypesUser:number;

    @Column()
    abbreviationTypeUser:string;

    @Column()
    nameTypeUser:string;

    @Column()
    descriptionTypeUser:string;

    @Column({nullable:true})
    permissionsProfileUser:string;

    @OneToMany(()=>UserEntity,(user)=>user.typeUser)
    user: UserEntity[];
}
