import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { TypeUserDto, UpdateTypeUserDto } from '../dto/type-user.dto';
import { TypesUserEntity } from '../entities/types-user.entity';

@Injectable()
export class TypesUserService {
    constructor(
        @InjectRepository(TypesUserEntity)
        private readonly typeUserRepository: Repository<TypesUserEntity>,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>
    ) { }

    async getAll(){
        return await this.typeUserRepository.find();
    }

    async getOne(idTypesUser: number) {
        const typeUser = await this.typeUserRepository.findOne(idTypesUser);
        return typeUser;
    }

    async getOneByName(nameTypeUser: string) {
        const typeUser = await this.typeUserRepository.findOne({nameTypeUser});
        return typeUser;
    }

    async getOneWithIdUser(idUser: number) {
        const user = await this.userRepository.findOne({idUser},{
            relations: ["typeUser"]
        });
        const idTypesUser = user.typeUser;
        const typeUser = await this.typeUserRepository.findOne(idTypesUser);
        return typeUser;
    }

    async create(data:TypeUserDto){
        const newTypeUser = new TypesUserEntity();
        newTypeUser.abbreviationTypeUser = data.abbreviationTypeUser;
        newTypeUser.nameTypeUser = data.nameTypeUser;
        newTypeUser.descriptionTypeUser = data.descriptionTypeUser;
        newTypeUser.permissionsProfileUser = data.permissionsProfileUser;
        return await this.typeUserRepository.save(newTypeUser)
    }

    async update(data: UpdateTypeUserDto){
        const typeUserFind = await this.typeUserRepository.findOne(data.idTypesUser);
        if(typeUserFind){
            this.typeUserRepository.merge(typeUserFind, data);
        }
        return await this.typeUserRepository.save(typeUserFind);
    }

    async delete(idTypeUser: number){
        return await this.typeUserRepository.delete(idTypeUser);
    }
}
