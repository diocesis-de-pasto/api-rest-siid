import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { TypeUserDto, UpdateTypeUserDto } from '../dto/type-user.dto';
import { TypesUserService } from '../services/types-user.service';

@ApiTags('types-user')
@Controller('types-user')
export class TypesUserController {
    constructor(
        private readonly typesUserService: TypesUserService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ){}

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar tipo de usuario')
    @Get()
    @ApiBearerAuth('Authorization')
    async all(@Req() req){
        if(req.admin){
            return this.typesUserService.getAll();
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear tipo de usuario')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req,@Body() data: TypeUserDto){
        if(req.admin){
            const typeUserFind = await this.typesUserService.getOneByName(data.nameTypeUser)
            .catch(err=>this.logExcHandler.create(req, err));            
            if(!typeUserFind){
                const newTypeUser = await this.typesUserService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo crear el tipo de usuario',
                    }, HttpStatus.CONFLICT);
                })
                this.auditHandler.create(req, 'TypesUserEntity', JSON.stringify(null), JSON.stringify(newTypeUser));
                return new HttpException('El tipo de usuario se creó correctamente', HttpStatus.OK);
            }else{
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'Ya existe una tipo de usuario con ese nombre',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar tipo de usuario')
    @Put()
    @ApiBearerAuth('Authorization')
    async edit(@Req() req,@Body() data: UpdateTypeUserDto){
        if(req.admin){
            const typeUserFind = await this.typesUserService.getOne(data.idTypesUser)
            .catch(err=>this.logExcHandler.create(req, err));
            if(typeUserFind && data.idTypesUser!==undefined){
                const typeUser = await this.typesUserService.update(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo actualizar el tipo de usuario',
                    }, HttpStatus.CONFLICT);
                })
                this.auditHandler.create(req, 'TypesUserEntity', JSON.stringify(null), JSON.stringify(typeUser));
                return new HttpException('El tipo de usuario se actualizó correctamente', HttpStatus.OK);
            }else{
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: `No existe el tipo de usuario`,
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar tipo de usuario')
    @Delete(':idTypeUser')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idTypeUser') idTypeUser: number){
        if(req.admin){
            const typeUserFind = await this.typesUserService.getOne(idTypeUser)
            .catch(err=>this.logExcHandler.create(req, err));
            if (typeUserFind) {
                const institutionDelete = await this.typesUserService.delete(idTypeUser)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar el tipo de usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'UserEntity', JSON.stringify(typeUserFind), JSON.stringify(null));
                return new HttpException('El tipo de usuario se eliminó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El tipo de usuario no se encuentra registrado',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }
}
