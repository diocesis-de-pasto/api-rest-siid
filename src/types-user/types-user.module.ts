import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { TypesUserController } from './controllers/types-user.controller';
import { TypesUserEntity } from './entities/types-user.entity';
import { TypesUserService } from './services/types-user.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            TypesUserEntity,
            UserEntity,
            AuditHandlerEntity,
            LogExceptionHandlerEntity
        ])
    ],
    providers: [
        TypesUserService,
        AuditHandlerService,
        LogExceptionHandlerService
    ],
    controllers: [TypesUserController]
})
export class TypesUserModule {}
