import { ModuleEntity } from "src/module/entities/module.entity";
import { RolesEntity } from "src/roles/entities/roles.entity";
import { Column, Entity, JoinTable, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('permissions')
export class PermissionEntity {
    @PrimaryGeneratedColumn()
    idPermission:number;

    @Column({nullable:true})
    admin: boolean;

    @ManyToOne(()=>RolesEntity,(role)=>role.permission)
    roleIdPermission: RolesEntity;

    @ManyToOne(()=>ModuleEntity,(module)=>module.idModule)
    @JoinTable()
    moduleId: ModuleEntity;
}
