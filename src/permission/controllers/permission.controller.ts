import { Body, Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { PermissionService } from '../services/permission.service';

@ApiTags('permission')
@Controller('permission')
export class PermissionController {
    constructor(
        private readonly permissionService: PermissionService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar permisos')
    @Get(':roleId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('roleId', ParseIntPipe) roleId: number) {
        const permissionsFind = await this.permissionService.getPermissions(roleId)
        .catch(err=>this.logExcHandler.create(req, err));
        if (permissionsFind) {
            return permissionsFind;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los permisos no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar permiso')
    @Put(':idRole')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data, @Param('idRole', ParseIntPipe) idRole: number) {
        var errorCount = 0;
        if(!req.admin){
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }        
        
        if(data.crud.find(item=>item.selected===1)) {
            const permissionsModule = await this.permissionService.getPermissionsModule(data.id,idRole);
            if(permissionsModule.find(item=>item===1)===undefined){
                await this.permissionService.create({admin: data.admin,roleIdPermission: idRole,moduleId: data.id})
                .then(newPermission=>{
                    this.auditHandler.create(req, 'PermissionEntity', null, JSON.stringify(newPermission));                
                })
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    errorCount++;
                })
            }else{
                await this.permissionService.update(data.id,idRole,{admin: data.admin,roleIdPermission: idRole,moduleId: data.id})
                .then(updatePermission=>{
                    this.auditHandler.create(req, 'PermissionEntity', JSON.stringify({idPermission: updatePermission.idPermission,admin: data.admin,roleIdPermission: idRole,moduleId: data.id}), JSON.stringify(updatePermission));
                })
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    errorCount++;
                })
            }
            var count = 0;
            var crud = ['','C','R','U','D'];
            data.crud.forEach(async item=>{
                var update: boolean = true;
                count++;
                if(item.type===crud[count] && item.selected!==permissionsModule[count]) {
                    if(item.selected===1) {
                        await this.permissionService.create({admin: data.admin,roleIdPermission: idRole,moduleId: item.id})
                        .then(newPermission=>{
                            this.auditHandler.create(req, 'PermissionEntity', null, JSON.stringify(newPermission));  
                        })
                        .catch(err => {
                            this.logExcHandler.create(req, err);
                            errorCount++;
                        })
                    }else{
                        await this.permissionService.delete(item.id,idRole)
                        .then((res)=>{
                            this.auditHandler.create(req, 'PermissionEntity', JSON.stringify({idPermission: item.id}), JSON.stringify(null));
                        })
                        .catch(err => {
                            this.logExcHandler.create(req, err);
                            errorCount++;
                        })
                    }
                    update = false;
                }
                if(update && permissionsModule[count]===1) {
                    await this.permissionService.update(item.id,idRole,{admin: data.admin,roleIdPermission: idRole,moduleId: item.id})
                    .then(updatePermission=>{
                        this.auditHandler.create(req, 'PermissionEntity', JSON.stringify({idPermission: updatePermission.idPermission,admin: data.admin,roleIdPermission: idRole,moduleId: item.id}), JSON.stringify(updatePermission));
                    })
                    .catch(err => {
                        this.logExcHandler.create(req, err);
                        errorCount++;
                    })
                }
            })
        }else {
            const permissionsModule = await this.permissionService.getPermissionsModule(data.id,idRole);
            if(permissionsModule.find(item=>item===1)!==undefined){
                data.crud.forEach(async item => {
                    try {
                        await this.permissionService.delete(item.id,idRole)
                    } catch (error) {}
                })
            }
            if(permissionsModule[0]===1){
                await this.permissionService.delete(data.id,idRole)
                .then(async ()=>{
                    this.auditHandler.create(req, 'PermissionEntity', JSON.stringify({idPermission: data.id}), JSON.stringify(null));
                })
                .catch(err => {
                    this.logExcHandler.create(req, err)
                })
            }
        }
        if(errorCount===0){
            return new HttpException('Los permisos se actualizaron correctamente', HttpStatus.OK);
        }else{
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Se presentaron algunos errores',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
