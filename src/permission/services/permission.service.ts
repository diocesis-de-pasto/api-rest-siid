import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ModuleEntity } from 'src/module/entities/module.entity';
import { RolesEntity } from 'src/roles/entities/roles.entity';
import { Repository } from 'typeorm';
import { PermissionEntity } from '../entities/permission.entity';

@Injectable()
export class PermissionService {
    constructor(
        @InjectRepository(PermissionEntity)
        private readonly permissionRepository: Repository<PermissionEntity>,
        @InjectRepository(RolesEntity)
        private readonly rolesRepository: Repository<RolesEntity>,
        @InjectRepository(ModuleEntity)
        private readonly moduleRepository: Repository<ModuleEntity>,
    ) { }

    async getPermissions(roleId: number) {
        const role = await this.rolesRepository.findOne({idRole:roleId});
        var permissions = await this.permissionRepository.find({
            relations: ["moduleId","roleIdPermission"]
        })
        permissions = permissions.filter(item=>item.roleIdPermission.idRole===roleId)
        const allModules = await this.moduleRepository.find();
        const parentModules = allModules.filter(item=>item.refModule==='0');
        const roleTable = [];
        
        parentModules.forEach(parentModule=>{
            const permissionModule = permissions.find(itemPer=>itemPer.moduleId.idModule===parentModule.idModule);
            const role = {
                id: parentModule.idModule,
                nombre: parentModule.nameModule,
                admin: permissionModule?permissionModule.admin:false
            }
            
            const childModules = allModules.filter(itemAllModule=>itemAllModule.refModule===parentModule.idModule.toString())
            role["crud"] = [
                {
                    id: childModules.find(itemChild=>itemChild.crud==='C')?childModules.find(itemChild=>itemChild.crud==='C').idModule:-1,
                    selected: this.selectedFunction(permissions,childModules,'C'),
                    type: 'C'
                },
                {
                    id: childModules.find(itemChild=>itemChild.crud==='R')?childModules.find(itemChild=>itemChild.crud==='R').idModule:-1,
                    selected: this.selectedFunction(permissions,childModules,'R'),
                    type: 'R'
                },
                {
                    id: childModules.find(itemChild=>itemChild.crud==='U')?childModules.find(itemChild=>itemChild.crud==='U').idModule:-1,
                    selected: this.selectedFunction(permissions,childModules,'U'),
                    type: 'U'
                },
                {
                    id: childModules.find(itemChild=>itemChild.crud==='D')?childModules.find(itemChild=>itemChild.crud==='D').idModule:-1,
                    selected: this.selectedFunction(permissions,childModules,'D'),
                    type: 'D'
                },
            ]
            roleTable.push(role);
        })
        return roleTable;
    }

    selectedFunction(permissions,childModules,type){
        const aux = (childModules.find(itemChild=>itemChild.crud===type)?childModules.find(itemChild=>itemChild.crud===type).idModule:-1)
        const a = permissions.find(itemPermission=>itemPermission.moduleId.idModule===aux)
        return a?1:0;
    }

    async getPermissionsModule(idModule:number,idRole:number) {
        var permissionsModule = [];
        const permissionsRole = await this.permissionRepository.find({
            relations: ["moduleId","roleIdPermission"]
        })
        const parentPermissionExists = permissionsRole.find(item=>item.moduleId.idModule===idModule&&item.roleIdPermission.idRole===idRole);
        const currentPermissions = permissionsRole.filter(item=>Number(item.moduleId.refModule)===idModule&&item.roleIdPermission.idRole===idRole);
        if(parentPermissionExists){
            permissionsModule.push(1);
        }else{
            permissionsModule.push(0);
        }
        if(currentPermissions.length>0) {
            currentPermissions.find(item=>item.moduleId.crud==='C')?permissionsModule.push(1):permissionsModule.push(0);
            currentPermissions.find(item=>item.moduleId.crud==='R')?permissionsModule.push(1):permissionsModule.push(0);
            currentPermissions.find(item=>item.moduleId.crud==='U')?permissionsModule.push(1):permissionsModule.push(0);
            currentPermissions.find(item=>item.moduleId.crud==='D')?permissionsModule.push(1):permissionsModule.push(0);
        }else{
            permissionsModule = [0,0,0,0,0];
        }
        return permissionsModule;
    }

    async create(data: any) {
        return await this.permissionRepository.save(data);
    }

    async update(idModule: number, idRole: number, data: any) {
        const permissions = await this.permissionRepository.find({
            relations: ["moduleId","roleIdPermission"]
        })        
        const permissionUpdate = permissions.find(item=>item.moduleId.idModule===idModule&&item.roleIdPermission.idRole===Number(idRole));
        var updatePermission = await this.permissionRepository.findOne(permissionUpdate.idPermission);
        this.permissionRepository.merge(updatePermission, data);
        return await this.permissionRepository.save(updatePermission);
    }

    async delete(idModule: number, idRole:number) {
        const permissions = await this.permissionRepository.find({
            relations: ["moduleId","roleIdPermission"]
        })
        const permissionDelete = permissions.find(item=>item.moduleId.idModule===idModule&&item.roleIdPermission.idRole===Number(idRole));
        return await this.permissionRepository.delete(permissionDelete.idPermission);
    }
}
