import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { ModuleEntity } from 'src/module/entities/module.entity';
import { RolesEntity } from 'src/roles/entities/roles.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { PermissionController } from './controllers/permission.controller';
import { PermissionEntity } from './entities/permission.entity';
import { PermissionService } from './services/permission.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            PermissionEntity,
            RolesEntity,
            UserEntity,
            AuditHandlerEntity,
            LogExceptionHandlerEntity,
            PermissionEntity,
            ModuleEntity
        ])
    ],
    providers: [
        PermissionService,
        AuditHandlerService,
        LogExceptionHandlerService,
        PermissionService
    ],
    controllers: [PermissionController]
})
export class PermissionModule {}
