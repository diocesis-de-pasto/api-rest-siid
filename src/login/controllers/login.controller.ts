import { Body, Controller, Get, HttpStatus, Post, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { LocalAuthGuard } from 'src/auth/guards/local-auth-guard';
import { AuthService } from 'src/auth/services/auth.service';
import { LoginDto } from '../dto/login.dto';

@ApiTags('login')
@Controller('login')
export class LoginController {
    constructor(
        private readonly authService: AuthService,
    ){}

    @UseGuards(LocalAuthGuard)
    @Post()
    @ApiOperation({summary: 'Endpoint para que el usuario ingrese, retorna el token'})
    async login(@Req() req,@Res() res,@Body() user:LoginDto) {
        const token = await this.authService.login(req.user);
        if(token){
            res.status(HttpStatus.OK).json(token);
        }else{
            res.status(HttpStatus.NO_CONTENT).json("Error al autenticar");
        }
    }

    @UseGuards(JwtAuthGuard)
    @Get('validateToken')
    @ApiBearerAuth('Authorization')
    getProfile(@Res() res) {
        res.status(HttpStatus.OK).json({message:true});
    }
}