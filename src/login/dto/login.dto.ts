import { IsEmail, IsNotEmpty, IsString, Length, MinLength } from "class-validator";
import { ApiProperty, PartialType } from "@nestjs/swagger";

export class LoginDto {
    @IsString()
    @IsEmail()
    @ApiProperty({description:'Email del usuario'})
    readonly email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    @ApiProperty({description: 'Contraseña de la cuenta de 6 caracteres'})
    readonly password: string;
}

export class UpdateUserDto extends PartialType(LoginDto) {}