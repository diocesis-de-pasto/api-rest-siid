import { forwardRef, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { AuthService } from './services/auth.service';
import { UserModule } from 'src/user/user.module';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { TypesUserService } from 'src/types-user/services/types-user.service';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';

@Module({
    imports: [
        PassportModule,
        JwtModule.registerAsync({
            inject: [ConfigService],
            useFactory: (config: ConfigService) => ({
            secret: config.get<string>('SECRET_JWT'),
                signOptions: { expiresIn: '14400s' },
            })
        }),
        forwardRef(()=>UserModule),
        TypeOrmModule.forFeature([UserEntity,TypesUserEntity])
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy, TypesUserService],
    exports: [AuthService,TypesUserService],
})
export class AuthModule {}
