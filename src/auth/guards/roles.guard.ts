import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { Observable } from 'rxjs';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RolesGuard implements CanActivate {
  private request;

  constructor(private reflector: Reflector,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>) { }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    this.request = context.switchToHttp().getRequest();
    const user = this.request.user;

    const rolesUser = this.userRepository.createQueryBuilder("users")
      .leftJoinAndSelect("users.role", "roleIdPermission")
      .leftJoinAndSelect("roleIdPermission.permission", "permissions")
      .leftJoinAndSelect("permissions.moduleId", "modules")
      .where("users.idUser=:id", { id: user.idUser })
      .getOne()

    return this.matchRoles(roles, rolesUser);
  }

  async matchRoles(roles, userPromise) {
    const groupUser = await userPromise;
    var permissionVerification = undefined;
    if(groupUser){
      permissionVerification = groupUser.role.permission.find(item=>item.moduleId.nameModule===roles[0]);
      this.request["admin"] = permissionVerification?permissionVerification.admin:false;
    }
    
    if (permissionVerification) {
      return true;
    }
    return false;
  }
}