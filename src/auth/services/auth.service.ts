import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/services/user.service';
import { compare } from "bcryptjs";
import { UserEntity } from 'src/user/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypesUserService } from 'src/types-user/services/types-user.service';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UserService,
        private jwtService: JwtService,
        private typesUser: TypesUserService,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>
    ) {}

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.usersService.findOneEmail(email);
        var verifyPassword;
        try {
            verifyPassword = await compare(pass, user.password);
        } catch (error) {
            verifyPassword = false;
        }
        if (user && verifyPassword && user.state==='enable') {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async login(user: UserEntity) {
        const typeUserPermission = await this.typesUser.getOneWithIdUser(user.idUser);
        const result = await this.userRepository.createQueryBuilder("users")
            .leftJoinAndSelect("users.role", "roleIdPermission")
            .leftJoinAndSelect("roleIdPermission.permission", "permissions")
            .leftJoinAndSelect("permissions.moduleId", "modules")
            .where("users.idUser=:id", { id: user.idUser })
            .getOne()
            .then(function(res){
                var json = [];
                var children = [];
                var routes = [];
                var routesChild; 
                const permissions = res.role.permission.filter(item=>{
                    if(item.moduleId.typeModule==='sub'){
                        return item;
                    }
                })
                for (let i = 0; i < permissions.length; i++) {
                    children = [0,0,0,0,0];
                    routesChild = [];
                    if(permissions[i].admin){
                        children[0] = 1;
                    }
                    for (let j = 0; j < res.role.permission.length; j++) {
                        if (permissions[i].moduleId.idModule===parseInt(res.role.permission[j].moduleId.refModule)) {
                            switch(res.role.permission[j].moduleId.crud){
                                case 'C':
                                    children[1] = 1;
                                    if(res.role.permission[j].moduleId.urlModule!==null){
                                        routesChild.push(`/session/${res.role.permission[j].moduleId.urlModule}`);
                                    }
                                    break;
                                case 'R':
                                    children[2] = 1;
                                    if(res.role.permission[j].moduleId.urlModule!==null){
                                        routesChild.push(`/session/${res.role.permission[j].moduleId.urlModule}`);
                                    }
                                    break;
                                case 'U':
                                    children[3] = 1;
                                    if(res.role.permission[j].moduleId.urlModule!==null){
                                        routesChild.push(`/session/${res.role.permission[j].moduleId.urlModule}`);
                                    }
                                    break;
                                case 'D':
                                    children[4] = 1;
                                    if(res.role.permission[j].moduleId.urlModule!==null){
                                        routesChild.push(`/session/${res.role.permission[j].moduleId.urlModule}`);
                                    }
                                    break;
                            }
                        }
                    }
                    if(permissions[i].moduleId.urlModule!==null && permissions[i].admin){
                        routes.push({
                            link: (`/session/${permissions[i].moduleId.urlModule}`),
                            icon: permissions[i].moduleId.iconModule,
                            title: permissions[i].moduleId.nameModule,
                            type: 'link',
                            routesChild
                        })
                    }
                    var tempJson = {};
                    tempJson["title"] = permissions[i].moduleId.nameModule;
                    tempJson["permissions"] = children;
                    json.push(tempJson)
                }
                if(res.numberOfAdmissions===0){
                    routes.push({
                        routesChild: ['/session/password']
                    })
                }
                return {modules:Buffer.from(JSON.stringify(json)).toString('base64'),routes}
            })
        const payload = { 
            email: user.email,
            sub: user.idUser,
            modules: result.modules,
            profilePermissions: JSON.parse(typeUserPermission.permissionsProfileUser),
            routes: result.routes
        };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
