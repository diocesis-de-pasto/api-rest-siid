export default () => ({
    port: parseInt(process.env.PORT, 10) || 3000,
    database: {
        host: process.env.DATABASE_HOST,
        port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        namedb: process.env.DATABASE_NAME,
        synchronize: process.env.TYPEORM_SYNCHRONIZE,
        logging: process.env.TYPEORM_LOGGING,
        entities: process.env.TYPEORM_ENTITIES
    },
    typeORM: {
        migrations: process.env.TYPEORM_MIGRATIONS,
        migrationsDir: process.env.TYPEORM_MIGRATIONS_DIR,
        migrationsTableName: process.env.TYPEORM_MIGRATIONS_TABLE_NAME
    },
    pathddp: process.env.PATHDDP
});