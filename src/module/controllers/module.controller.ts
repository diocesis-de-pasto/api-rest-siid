import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { ModuleCreateDto, UpdateModuleDto } from '../dto/module-create.dto';
import { ModuleService } from '../services/module.service';

@ApiTags('module')
@Controller('module')
export class ModuleController {
    constructor(private readonly moduleService: ModuleService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar módulos')
    @Get(':moduleId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('moduleId', ParseIntPipe) moduleId: number) {
        const moduleFind = await this.moduleService.getOne(moduleId)
        .catch(err=>this.logExcHandler.create(req, err));
        if (moduleFind) {
            return moduleFind;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El módulo no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar módulos')
    @Get()
    @ApiBearerAuth('Authorization')
    async getAll(@Req() req) {
        const modules = await this.moduleService.getAll()
        .catch(err=>this.logExcHandler.create(req, err));
        if (modules) {
            return modules;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los módulos no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear módulo')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: ModuleCreateDto) {
        const module = await this.moduleService.findByName(data.nameModule)
        .catch(err=>this.logExcHandler.create(req, err));
        if (!module) {
            const newModule = await this.moduleService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo crear el módulo',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'ModuleEntity', JSON.stringify(null), JSON.stringify(newModule));
            return new HttpException('El módulo se creó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El módulo ya está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar módulo')
    @Put(':idModule')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateModuleDto, @Param('idModule') idModule: number) {
        const module = await this.moduleService.getOne(idModule)
        .catch(err=>this.logExcHandler.create(req, err));
        if (module) {
            const moduleUpdate = await this.moduleService.update(module.idModule, data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo actualizar el módulo',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'ModuleEntity', JSON.stringify(module), JSON.stringify(moduleUpdate));
            return new HttpException('El módulo se actualizó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El módulo no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar módulo')
    @Delete(':idModule')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idModule') idModule: number) {
        if(req.admin){
            const module = await this.moduleService.getOne(idModule)
            .catch(err=>this.logExcHandler.create(req, err));
            if (module) {
                const moduleDelete = await this.moduleService.delete(idModule)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar el módulo',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'ModuleEntity', JSON.stringify(module), JSON.stringify(null));
                return new HttpException('El módulo se eliminó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El módulo no se encuentra registrado',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }
}
