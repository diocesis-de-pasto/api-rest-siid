import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { ModuleController } from './controllers/module.controller';
import { ModuleEntity } from './entities/module.entity';
import { ModuleService } from './services/module.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ModuleEntity,
            UserEntity, 
            AuditHandlerEntity, 
            LogExceptionHandlerEntity,
        ]),
    ],
    providers: [
        ModuleService,
        AuditHandlerService, 
        LogExceptionHandlerService,
    ],
    controllers: [ModuleController]
})
export class ModuleModule {}
