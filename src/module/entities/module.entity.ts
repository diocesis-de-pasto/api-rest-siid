
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('modules')
export class ModuleEntity {
    @PrimaryGeneratedColumn()
    idModule:number;

    @Column()
    nameModule: string;
    
    @Column()
    typeModule: string;

    @Column()
    refModule: string;
    
    @Column({nullable:true})
    urlModule: string;
    
    @Column({nullable:true})
    iconModule: string;
    
    @Column()
    abbreviationModule: string;

    @Column()
    description: string;

    @Column({nullable:true})
    crud: string;
}
