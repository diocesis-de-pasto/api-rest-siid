import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class ModuleCreateDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    nameModule: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    typeModule: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    refModule: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    urlModule: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    iconModule: string;
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    abbreviationModule: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    description: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    crud: string;
}

export class UpdateModuleDto extends PartialType(ModuleCreateDto) {}