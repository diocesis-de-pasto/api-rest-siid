import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ModuleCreateDto, UpdateModuleDto } from '../dto/module-create.dto';
import { ModuleEntity } from '../entities/module.entity';

@Injectable()
export class ModuleService {
    constructor(
        @InjectRepository(ModuleEntity)
        private readonly moduleRepository: Repository<ModuleEntity>,
    ) { }

    async getOne(idModule: number) {
        const module = await this.moduleRepository.findOne( idModule );
        return module;
    }

    async getAll() {
        const modules = await this.moduleRepository.find();
        return modules;
    }

    async findByName(nameModule: string) {
        return await this.moduleRepository.findOne({ nameModule });
    }

    async create(data: ModuleCreateDto) {
        return await this.moduleRepository.save(data);
    }

    async update(idModule: number, data: UpdateModuleDto) {
        var updateModule = await this.moduleRepository.findOne(idModule);
        this.moduleRepository.merge(updateModule, data);
        return await this.moduleRepository.save(updateModule);
    }

    async delete(idModule: number) {
        return await this.moduleRepository.delete(idModule);
    }
}
