import { Controller, Get, HttpException, HttpStatus, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { StatisticsService } from '../services/statistics.service';

@ApiTags('statistics')
@Controller('statistics')
export class StatisticsController {
    constructor(
        private readonly statisticsService: StatisticsService
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar estadísticas')
    @Get('ordinationTime')
    @ApiBearerAuth('Authorization')
    async getOrdinationTime(@Req() req) {
        if(!req.admin){
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
        const times = await this.statisticsService.getOrdinationTime();
        var cont05 = 0,
            cont610 = 0,
            cont1120 = 0,
            cont2130 = 0,
            cont3140 = 0,
            cont4150 = 0,
            cont50plus = 0;
        if (times) {
            times.forEach(item => {
                if(item.priestlyOrdinationDate!==null){
                    const difTime = new Date().getTime() - item.priestlyOrdinationDate.getTime();
                    const difTimeInt = Math.floor(difTime/(1000*60*60*24*365));
                    if(difTimeInt>=0 && difTimeInt<=5) cont05++;
                    if(difTimeInt>=6 && difTimeInt<=10) cont610++;
                    if(difTimeInt>=11 && difTimeInt<=20) cont1120++;
                    if(difTimeInt>=21 && difTimeInt<=30) cont2130++;
                    if(difTimeInt>=31 && difTimeInt<=40) cont3140++;
                    if(difTimeInt>=41 && difTimeInt<=50) cont4150++;
                    if(difTimeInt>=51 && difTimeInt<=120) cont50plus++;
                }
            })

            return {
                "0-5": cont05,
                "6-10": cont610,
                "11-20": cont1120,
                "21-30": cont2130,
                "31-40": cont3140,
                "41-50": cont4150,
                "51+": cont50plus
            };
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'No se ha podido encontrar la información',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar estadísticas')
    @Get('getAges')
    @ApiBearerAuth('Authorization')
    async getAges(@Req() req) {
        if(!req.admin){
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
        const ages = await this.statisticsService.agesPriests();
        var cont2530 = 0,
            cont3140 = 0,
            cont4150 = 0,
            cont5160 = 0,
            cont6170 = 0,
            cont7180 = 0,
            cont8190 = 0,
            cont91100 = 0,
            cont101110 = 0,
            cont111120 = 0;
        if (ages) {
            ages.forEach(item => {
                if(item.dateOfBirth!==null){
                    const difTime = new Date().getTime() - item.dateOfBirth.getTime();
                    const difTimeInt = Math.floor(difTime/(1000*60*60*24*365));
                    if(difTimeInt>=25 && difTimeInt<=30) cont2530++;
                    if(difTimeInt>=31 && difTimeInt<=40) cont3140++;
                    if(difTimeInt>=41 && difTimeInt<=50) cont4150++;
                    if(difTimeInt>=51 && difTimeInt<=60) cont5160++;
                    if(difTimeInt>=61 && difTimeInt<=70) cont6170++;
                    if(difTimeInt>=71 && difTimeInt<=80) cont7180++;
                    if(difTimeInt>=81 && difTimeInt<=90) cont8190++;
                    if(difTimeInt>=91 && difTimeInt<=100) cont91100++;
                    if(difTimeInt>=101 && difTimeInt<=110) cont101110++;
                    if(difTimeInt>=111 && difTimeInt<=120) cont111120++;
                }
            })

            return {
                "25-30": cont2530,
                "31-40": cont3140,
                "41-50": cont4150,
                "51-60": cont5160,
                "61-70": cont6170,
                "71-80": cont7180,
                "81-90": cont8190,
                "91-100": cont91100,
                "101-110": cont101110,
                "111-120": cont111120,
            };
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'No se ha podido encontrar la información',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar estadísticas')
    @Get('studiesPriest')
    @ApiBearerAuth('Authorization')
    async getStudiesPriest(@Req() req) {
        if(!req.admin){
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
        const studies = await this.statisticsService.numberStudiesPriest();
        var primary = 0,
            secondary = 0,
            ecclesiastical = 0,
            academic = 0,
            undergraduate = 0,
            graduate = 0,
            diplomat = 0,
            course = 0,
            technical = 0,
            technological = 0,
            seminar = 0;
        if (studies) {
            studies.forEach(item => {
                if(item.levelOfStudy==='Primarios') primary++;
                if(item.levelOfStudy==='Secundarios') secondary++;
                if(item.levelOfStudy==='Eclesiasticos') ecclesiastical++;
                if(item.levelOfStudy==='Universitario') academic++;
                if(item.levelOfStudy==='Pregrado') undergraduate++;
                if(item.levelOfStudy==='Postgrado') graduate++;
                if(item.levelOfStudy==='Diplomado') diplomat++;
                if(item.levelOfStudy==='Curso') course++;
                if(item.levelOfStudy==='Técnico') technical++;
                if(item.levelOfStudy==='Tecnológico') technological++;
                if(item.levelOfStudy==='Seminario') seminar++;
            })

            return {
                "primary": primary,
                "secondary": secondary,
                "ecclesiastical": ecclesiastical,
                "academic": academic,
                "undergraduate": undergraduate,
                "graduate": graduate,
                "diplomat": diplomat,
                "course": course,
                "technical": technical,
                "technological": technological,
                "seminar": seminar
            };
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'No se ha podido encontrar la información',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar estadísticas')
    @Get('serviceTime')
    @ApiBearerAuth('Authorization')
    async getServiceTime(@Req() req) {
        if(!req.admin){
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
        const times = await this.statisticsService.serviceTime();
        if (times) {
            return times;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'No se ha podido encontrar la información',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
