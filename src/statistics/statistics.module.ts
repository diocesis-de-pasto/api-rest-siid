import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MinisterialInformationEntity } from 'src/ministerial-information/entities/ministerial-information.entity';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { DataPersonEntity } from 'src/user/entities/data-person.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { StatisticsController } from './constrollers/statistics.controller';
import { StatisticsService } from './services/statistics.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MinisterialInformationEntity,
      UserEntity,
      DataPersonEntity,
      TypesUserEntity
    ]),
  ],
  providers: [
      StatisticsService
  ],
  controllers: [StatisticsController]
})
export class StatisticsModule {}
