import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MinisterialInformationEntity } from 'src/ministerial-information/entities/ministerial-information.entity';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { DataPersonEntity } from 'src/user/entities/data-person.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StatisticsService {
    constructor(
        @InjectRepository(MinisterialInformationEntity)
        private readonly ministerialInformationRepository: Repository<MinisterialInformationEntity>,
        @InjectRepository(DataPersonEntity)
        private readonly dataPersonRepository: Repository<DataPersonEntity>,
        @InjectRepository(TypesUserEntity)
        private readonly typesUserRepository: Repository<TypesUserEntity>,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
    ) { }

    async getOrdinationTime() {
        const dataMinisterial = await this.ministerialInformationRepository.find();
        return dataMinisterial;
    }

    async agesPriests() {
        var dates = [];
        const typeUserPriest = await this.typesUserRepository.findOne({nameTypeUser:'Sacerdote'});
        await this.userRepository.createQueryBuilder("users")
            .leftJoinAndSelect("users.typeUser", "idTypeUser")
            .leftJoinAndSelect("users.dataPerson", "idDataPerson")
            .where("users.typeUser=:id", { id: typeUserPriest.idTypesUser })
            .getMany()
            .then(res=>{
                res.forEach(item => {
                    const {dataPerson,...other} = item;
                    if(dataPerson && dataPerson.dateOfBirth){
                        dates.push(dataPerson);
                    }
                })
            })
        return dates;
    }

    async numberStudiesPriest() {
        var studies = [];
        const typeUserPriest = await this.typesUserRepository.findOne({nameTypeUser:'Sacerdote'});
        await this.userRepository.createQueryBuilder("users")
            .leftJoinAndSelect("users.dataAcademy", "idDataAcademy")
            .leftJoinAndSelect("users.typeUser", "idTypeUser")
            .where("users.typeUser=:id", { id: typeUserPriest.idTypesUser })
            .getMany()
            .then(res=>{
                res.forEach(item => {
                    const {dataAcademy,...other} = item;
                    dataAcademy.forEach(dataAcaUser=>{
                        if(dataAcaUser && dataAcaUser.state === 'Terminado'){
                            studies.push(dataAcaUser);
                        }
                    })
                })
            })
        return studies;
    }

    async serviceTime() {
        var studies = [];
        const typeUserPriest = await this.typesUserRepository.findOne({nameTypeUser:'Sacerdote'});
        await this.userRepository.createQueryBuilder("users")
            .leftJoinAndSelect("users.dataPastoral", "idDataPastoral")
            .leftJoinAndSelect("users.dataPerson", "idDataPerson")
            .leftJoinAndSelect("users.typeUser", "idTypeUser")
            .where("users.typeUser=:id", { id: typeUserPriest.idTypesUser })
            .getMany()
            .then(res=>{
                res.forEach(item => {
                    const {dataPastoral,dataPerson,...other} = item;
                    dataPastoral.forEach(dataPastoralUser=>{
                        if(dataPastoralUser && dataPastoralUser.serviceStartDate !== null && dataPastoralUser.serviceEndDate === null){
                            const difTime = new Date().getTime() - dataPastoralUser.serviceStartDate.getTime();
                            const difTimeInt = Math.floor(difTime/(1000*60*60*24*365));
                            var names = '';
                            var surnames = '';
                            var color = '';
                            if(difTimeInt>=0 && difTimeInt<=2){
                                color = "green";
                            }
                            if(difTimeInt>=3 && difTimeInt<=4){
                                color = "yellow";
                            }
                            if(difTimeInt>=5){
                                color = "red";
                            }
                            if(dataPerson && dataPerson.firstName){
                                names = `${dataPerson.firstName}${dataPerson.secondName?(" "+dataPerson.secondName):""}`;
                            }
                            if(dataPerson && dataPerson.firstSurname){
                                surnames = `${dataPerson.firstSurname}${dataPerson.secondSurname?(" "+dataPerson.secondSurname):""}`;
                            }
                            studies.push({
                                names,
                                surnames,
                                email: other.email,
                                identification: "C.C. "+other.documentId,
                                time: difTimeInt,
                                class: color
                            });
                        }
                    })
                })
            })
        return studies;
    }
}