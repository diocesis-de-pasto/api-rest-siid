import {
    IsNotEmpty,
    MinLength,
    MaxLength,
    IsEmail,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CedulaEmailCreateDto {
    @MinLength(6)
    @MaxLength(11)
    @IsNotEmpty()
    @ApiProperty()
    readonly documentId: string;

    @IsNotEmpty()
    @IsEmail()
    @ApiProperty()
    readonly email: string;
}