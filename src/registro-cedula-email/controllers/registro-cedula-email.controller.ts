import { Body, Controller, HttpException, HttpStatus, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UserCreateDto } from 'src/user/dto/user-create.dto';
import { UserService } from 'src/user/services/user.service';

@ApiTags('registro-cedula-email')
@Controller('registro-cedula-email')
export class RegistroCedulaEmailController {

    constructor(private readonly userService: UserService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ) { }

    @Post()
    async createUserDocumentEmail(@Body() data: UserCreateDto) {
        const userFind = await this.userService.findOneEmail(data.email)
        .catch(err=>console.log("Error crear usuario documento 1",err));
        if (!userFind) {
            const user = await this.userService.create(data)
                .catch(err => {
                    // this.logExcHandler.create(req, err)
                    console.log("Error crear usuario documento 2",err);
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo crear el usuario',
                    }, HttpStatus.CONFLICT);
                })
            return new HttpException('El usuario se creó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario ya está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
