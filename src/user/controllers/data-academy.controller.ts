import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UserEntity } from '../entities/user.entity';
import { UserService } from '../services/user.service';
import { DataAcademyService } from '../services/data-academy.service';
import { DataAcademyCreateDto, UpdateDataAcademyDto } from '../dto/data-academy-create.dto';

@ApiTags('dataAcademy')
@Controller('data-academy')
export class DataAcademyController {
    constructor(private readonly dataAcademyService: DataAcademyService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService,
        private readonly userService: UserService,
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar datos académicos')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const dataAcademy = await this.dataAcademyService.getDataAcademy(user)
            .catch(err => this.logExcHandler.create(req, err));
        if (dataAcademy) {
            if(req.user.idUser!==userId){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return dataAcademy;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los datos académicos del usuario no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear datos académicos')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: DataAcademyCreateDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const dataAcademyNew = await this.dataAcademyService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo guardar los nuevos datos académicos del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'DataAcademyEntity', JSON.stringify(null), JSON.stringify(dataAcademyNew));
            return new HttpException('Los nuevos datos académicos del usuario se guardaron correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar datos académicos')
    @Put(':idDataAcademy')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateDataAcademyDto, @Param('idDataAcademy', ParseIntPipe) idDataAcademy: number) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const dataAcademy = await this.dataAcademyService.getDataAcademy(user)
                .catch(err => this.logExcHandler.create(req, err));                
            if (dataAcademy && dataAcademy.find(res=> res.idDataAcademy===idDataAcademy)) {
                const dataAcademyUpdate = await this.dataAcademyService.update(idDataAcademy, data)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo actualizar los datos del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'DataAcademyEntity', JSON.stringify(dataAcademy), JSON.stringify(dataAcademyUpdate));
                return new HttpException('El usuario se actualizó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no ha creado los datos',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar datos académicos')
    @Delete(':idDataAcademy')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idDataAcademy', ParseIntPipe) idDataAcademy: number, @Body() data: DataAcademyCreateDto) {
        if(req.admin){
            const userFind = await this.userService.getOne(data.user.idUser)
                .catch(err => this.logExcHandler.create(req, err));
            if (userFind) {
                const user = new UserEntity();
                user.idUser = userFind.idUser;
                const dataAcademy = await this.dataAcademyService.getDataAcademy(user)
                    .catch(err => this.logExcHandler.create(req, err))
                if (dataAcademy && dataAcademy.find(res => res.idDataAcademy === idDataAcademy)) {
                    const dataAcademyDelete = await this.dataAcademyService.delete(idDataAcademy)
                        .catch(err => {
                            this.logExcHandler.create(req, err)
                            throw new HttpException({
                                status: HttpStatus.CONFLICT,
                                error: 'No se pudo eliminar los datos del usuario',
                            }, HttpStatus.CONFLICT);
                        })
                    this.auditHandler.create(req, 'DataAcademyEntity', JSON.stringify(dataAcademy), JSON.stringify(null));
                    return new HttpException('Los datos del usuario se eliminaron correctamente', HttpStatus.OK);
                } else {
                    throw new HttpException({
                        status: HttpStatus.NO_CONTENT,
                        error: 'El usuario no tiene datos académicos guardados',
                    }, HttpStatus.NO_CONTENT);
                }
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no se encuentra registrado',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }
}
