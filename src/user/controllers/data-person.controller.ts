import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, Res, UploadedFile, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { DataPersonService } from '../services/data-person.service';
import { UserEntity } from '../entities/user.entity';
import { DataPersonCreateDto, UpdateDataPersonDto } from '../dto/data-person-create.dto';
import { UserService } from '../services/user.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { fileFilter, renameImage } from '../helpers/rename.helper';
import { diskStorage } from 'multer';
import * as fs from 'fs';

@ApiTags('dataPerson')
@Controller('dataPerson')
export class DataPersonController {
    constructor(private readonly dataPersonService: DataPersonService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService,
        private readonly userService: UserService,
    ) { }

    // @UseGuards(JwtAuthGuard, RolesGuard)
    // @Roles('Listar datos personales')
    @Get('getImage/:userId')
    @ApiBearerAuth('Authorization')
    async downloadFile(@Res() res,@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const dataPerson = await this.dataPersonService.getDataPerson(user)
            .catch(err => this.logExcHandler.create(req, err));
        if (dataPerson) {
            var searchImage;
            var image;
            try {
                searchImage = fs.existsSync(`./imagesUsers/${dataPerson.photo}`);
                if(searchImage){
                    image = fs.createReadStream(`./imagesUsers/${dataPerson.photo}`);
                    let buff = fs.readFileSync(`./imagesUsers/${dataPerson.photo}`);
                    image = buff.toString('base64');
                    image = `data:image/${dataPerson.photo.split('.')[1]};base64,${image}`;
                }else{
                    image = '';
                }
            } catch (error) {
                console.log("error ",error);
            }
            res.status(200).json({image});
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los datos del usuario no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar datos personales')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const dataPerson = await this.dataPersonService.getDataPerson(user)
            .catch(err => this.logExcHandler.create(req, err));
        if (dataPerson) {
            if(req.user.idUser!==userId){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return dataPerson;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los datos del usuario no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear datos personales')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: DataPersonCreateDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const dataPerson = await this.dataPersonService.getDataPerson(user)
                .catch(err => this.logExcHandler.create(req, err));
            if (!dataPerson) {
                const dataPerson = await this.dataPersonService.create(data, user)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo guardar los datos del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'DataPersonEntity', JSON.stringify(null), JSON.stringify(dataPerson));
                return new HttpException('Los datos del usuario se guardaron correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'Los datos ya fueron creados',
                }, HttpStatus.FORBIDDEN);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no está registrado',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear datos personales')
    @Post('uploadPhoto/:idUser')
    @UseInterceptors(FileInterceptor('file',{
        storage: diskStorage({
            destination: './imagesUsers',
            filename: renameImage
        }),
        fileFilter: fileFilter
    }))
    @ApiBearerAuth('Authorization')
    async uploadImage(@UploadedFile() file: Express.Multer.File, @Req() req, @Param('idUser', ParseIntPipe) idUser:number) {        
        const userFind = await this.userService.getOne(idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.CONFLICT);
                }
            }
            const dataPerson = await this.dataPersonService.getDataPerson(user)
                .catch(err => this.logExcHandler.create(req, err));
            if (dataPerson) {
                var fileName;
                try {
                    fileName = file.filename;
                } catch (error) {
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'Imagen no compatible',
                    }, HttpStatus.CONFLICT);
                }
                if(fs.existsSync(`./imagesUsers/${dataPerson.photo}`)){
                    fs.unlink(`./imagesUsers/${dataPerson.photo}`,(errorDeleteFile)=>{
                        if (errorDeleteFile) this.logExcHandler.create(req, errorDeleteFile.message);
                    });
                }                
                const dataPersonUpdate = await this.dataPersonService.updateImage(user.idUser, fileName)
                    .catch(err => {
                        fs.unlink(`./imagesUsers/${file.filename}`,(errorDeleteFile)=>{
                            if (errorDeleteFile) this.logExcHandler.create(req, errorDeleteFile.message);
                        });
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo actualizar la foto del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'DataPersonEntity', JSON.stringify(dataPerson), JSON.stringify(dataPersonUpdate));
                return new HttpException('El usuario se actualizó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no ha creado los datos',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar datos personales')
    @Put()
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateDataPersonDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const dataPerson = await this.dataPersonService.getDataPerson(user)
                .catch(err => this.logExcHandler.create(req, err));
            if (dataPerson) {
                const dataPersonUpdate = await this.dataPersonService.update(user.idUser, data)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo actualizar los datos del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'DataPersonEntity', JSON.stringify(dataPerson), JSON.stringify(dataPersonUpdate));
                return new HttpException('El usuario se actualizó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no ha creado los datos',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar datos personales')
    @Delete('deletePhoto/:idUser')
    @ApiBearerAuth('Authorization')
    async deleteImage(@Req() req, @Param('idUser') idUser: number) {
        const userFind = await this.userService.getOne(idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            if (req.user.idUser !== userFind.idUser) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            const dataPerson = await this.dataPersonService.getDataPerson(user)
                .catch(err => this.logExcHandler.create(req, err))
            if (dataPerson) {
                if(fs.existsSync(`./imagesUsers/${dataPerson.photo}`)){
                    fs.unlink(`./imagesUsers/${dataPerson.photo}`,(errorDeleteFile)=>{
                        if (errorDeleteFile) this.logExcHandler.create(req, errorDeleteFile.message);
                    });
                    const data = {photo:'null'};
                    const dataPersonUpdate = await this.dataPersonService.update(user.idUser, data)
                        .catch(err => {
                            this.logExcHandler.create(req, err)
                            throw new HttpException({
                                status: HttpStatus.CONFLICT,
                                error: 'No se pudo actualizar los datos del usuario',
                            }, HttpStatus.CONFLICT);
                        })
                    this.auditHandler.create(req, 'DataPersonEntity', JSON.stringify(dataPerson), JSON.stringify(null));
                    return new HttpException('La imagen del usuario se eliminó correctamente', HttpStatus.OK);
                }else{
                    throw new HttpException({
                        status: HttpStatus.NO_CONTENT,
                        error: 'El usuario no tiene registrada una imagen',
                    }, HttpStatus.NO_CONTENT);
                }
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no tiene datos personales guardados',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
        
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar datos personales')
    @Delete(':idUser')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idUser') idUser: number) {
        const userFind = await this.userService.getOne(idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            if (req.user.idUser !== userFind.idUser) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            const dataPerson = await this.dataPersonService.getDataPerson(user)
                .catch(err => this.logExcHandler.create(req, err))
            if (dataPerson) {
                const dataPersonDelete = await this.dataPersonService.delete(user.idUser)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar los datos del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                if(fs.existsSync(`./imagesUsers/${dataPerson.photo}`)){
                    fs.unlink(`./imagesUsers/${dataPerson.photo}`,(errorDeleteFile)=>{
                        if (errorDeleteFile) this.logExcHandler.create(req, errorDeleteFile.message);
                    });
                }  
                this.auditHandler.create(req, 'DataPersonEntity', JSON.stringify(dataPerson), JSON.stringify(null));
                return new HttpException('Los datos del usuario se eliminaron correctamente', HttpStatus.OK);

            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no tiene datos personales guardados',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
