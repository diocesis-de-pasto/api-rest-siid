import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UserEntity } from '../entities/user.entity';
import { UserService } from '../services/user.service';
import { DataPastoralService } from '../services/data-pastoral.service';
import { DataPastoralCreateDto, UpdateDataPastoralDto } from '../dto/data-pastoral.dto';

@ApiTags('dataPastoral')
@Controller('data-pastoral')
export class DataPastoralController {
    constructor(private readonly dataPastoralService: DataPastoralService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService,
        private readonly userService: UserService,
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar datos pastorales')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const dataPastoral = await this.dataPastoralService.getDataPastoral(user)
            .catch(err => this.logExcHandler.create(req, err));
        if (dataPastoral) {
            if(req.user.idUser!==userId){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return dataPastoral;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los datos pastorales del usuario no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear datos pastorales')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: DataPastoralCreateDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const dataPastoralNew = await this.dataPastoralService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo guardar los nuevos datos pastorales del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'DataPastoralEntity', JSON.stringify(null), JSON.stringify(dataPastoralNew));
            return new HttpException('Los nuevos datos pastorales del usuario se guardaron correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar datos pastorales')
    @Put(':idDataPastoral')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateDataPastoralDto, @Param('idDataPastoral', ParseIntPipe) idDataPastoral: number) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const dataPastoral = await this.dataPastoralService.getDataPastoral(user)
                .catch(err => this.logExcHandler.create(req, err));                
            if (dataPastoral && dataPastoral.find(res=> res.idDataPastoral===idDataPastoral)) {
                const dataPastoralUpdate = await this.dataPastoralService.update(idDataPastoral, data)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo actualizar los datos del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'DataPastoralEntity', JSON.stringify(dataPastoral), JSON.stringify(dataPastoralUpdate));
                return new HttpException('El usuario se actualizó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no ha creado los datos',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar datos pastorales')
    @Delete(':idDataPastoral')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idDataPastoral', ParseIntPipe) idDataPastoral: number, @Body() data: DataPastoralCreateDto) {
        if(req.admin){
            const userFind = await this.userService.getOne(data.user.idUser)
                .catch(err => this.logExcHandler.create(req, err));
            if (userFind) {
                const user = new UserEntity();
                user.idUser = userFind.idUser;
                const dataPastoral = await this.dataPastoralService.getDataPastoral(user)
                    .catch(err => this.logExcHandler.create(req, err))
                if (dataPastoral && dataPastoral.find(res => res.idDataPastoral === idDataPastoral)) {
                    const dataPastoralDelete = await this.dataPastoralService.delete(idDataPastoral)
                        .catch(err => {
                            this.logExcHandler.create(req, err)
                            throw new HttpException({
                                status: HttpStatus.CONFLICT,
                                error: 'No se pudo eliminar los datos del usuario',
                            }, HttpStatus.CONFLICT);
                        })
                    this.auditHandler.create(req, 'DataPastoralEntity', JSON.stringify(dataPastoral), JSON.stringify(null));
                    return new HttpException('Los datos del usuario se eliminaron correctamente', HttpStatus.OK);
    
                } else {
                    throw new HttpException({
                        status: HttpStatus.NO_CONTENT,
                        error: 'El usuario no tiene datos pastorales guardados o el ID es incorrecto',
                    }, HttpStatus.NO_CONTENT);
                }
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no se encuentra registrado',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }
}
