import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { UserService } from '../services/user.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UpdateUserDto, UserCreateDto } from '../dto/user-create.dto';
import { Roles } from 'src/auth/decorators/roles.decorator';

@ApiTags('user')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar usuario')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const userFind = await this.userService.getOne(userId)
        .catch(err=>this.logExcHandler.create(req, err));
        if (userFind) {
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return userFind;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se ha podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar usuario')
    @Get()
    @ApiBearerAuth('Authorization')
    async getAll(@Req() req) {
        if(req.admin){
            const users = await this.userService.getAll()
            .catch(err=>this.logExcHandler.create(req, err));
            if (users) {
                return users;
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no se ha podido encontrar',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear usuario')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: UserCreateDto) {
        const userFind = await this.userService.findOneEmail(data.email)
        .catch(err=>this.logExcHandler.create(req, err));
        if (!userFind) {
            // if(req.admin){
                const user = await this.userService.create(data)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo crear el usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'UserEntity', JSON.stringify(null), JSON.stringify(user));
                return new HttpException('El usuario se creó correctamente', HttpStatus.OK);
            // }else{
                // throw new HttpException({
                //     status: HttpStatus.UNAUTHORIZED,
                //     error: 'No tiene los permisos suficientes',
                // }, HttpStatus.UNAUTHORIZED);
            // }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario ya está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar usuario')
    @Put()
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateUserDto) {
        const userFind = await this.userService.findOneEmail(data.email)
        .catch(err=>this.logExcHandler.create(req, err));
        if (userFind) {
            var admin = false;
            if(req.user.idUser!==userFind.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const user = await this.userService.update(userFind.email, data, req.admin)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo actualizar el usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'UserEntity', JSON.stringify(userFind), JSON.stringify(user));
            return new HttpException('El usuario se actualizó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar usuario')
    @Delete(':idUser')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idUser') idUser: number) {
        if(req.admin){
            const userFind = await this.userService.getOne(idUser)
            .catch(err=>this.logExcHandler.create(req, err));
            if (userFind) {
                const user = await this.userService.delete(idUser)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar el usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'UserEntity', JSON.stringify(userFind), JSON.stringify(null));
                return new HttpException('El usuario se eliminó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no se encuentra registrado',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }
}
