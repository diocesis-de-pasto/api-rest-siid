import {
    IsNotEmpty,
    MinLength,
    MaxLength,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from '../entities/user.entity';

export class DataPersonCreateDto {
    @ApiProperty()
    readonly idDataPerson: number;

    @ApiProperty()
    readonly expeditionDocumentDate: string;

    @ApiProperty()
    readonly expeditionDocumentPlace: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly firstName: string;

    @ApiProperty()
    readonly secondName: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly firstSurname: string;

    @ApiProperty()
    readonly secondSurname: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly dateOfBirth: string;

    @ApiProperty()
    readonly cityOfBirth: string;

    @ApiProperty()
    readonly departmentOfBirth: string;

    @ApiProperty()
    readonly countryOfBirth: string;

    @ApiProperty()
    readonly gender: string;

    @ApiProperty({nullable:true})
    readonly photo: string;

    @ApiProperty({nullable:true})
    readonly residenceAddress: string;

    @ApiProperty()
    readonly bloodType: string;

    @ApiProperty()
    readonly contactNumberOne: string;

    @ApiProperty()
    readonly contactNumberTwo: string;

    @ApiProperty()
    readonly user: UserEntity;

}

export class UpdateDataPersonDto extends PartialType(DataPersonCreateDto) {}