import {
    IsNotEmpty,
    IsEmail,
    MinLength,
    MaxLength,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { DataPersonEntity } from '../entities/data-person.entity';
import { InstitutionEntity } from 'src/institution/entities/institution.entity';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { RolesEntity } from 'src/roles/entities/roles.entity';

export class UserCreateDto {
    @MinLength(7)
    @MaxLength(11)
    @IsNotEmpty()
    @ApiProperty()
    readonly documentId: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly documentType: string;

    @IsEmail()
    @IsNotEmpty()
    @ApiProperty()
    readonly email: string;

    @IsEmail()
    @IsNotEmpty()
    @ApiProperty()
    readonly emailCorporate: string;

    @ApiProperty()
    readonly state: string;
    
    @ApiProperty()
    readonly typeUser: number;
    
    @MinLength(8)
    @IsNotEmpty()
    @ApiProperty()
    readonly password: string;
    
    @ApiProperty()
    readonly institution: number;

    
    @ApiProperty()
    readonly role: number;
    
    // @ApiProperty()
    // readonly dataPerson: DataPersonEntity;
    
    // @ApiProperty()
    // readonly dataAcademy: DataAcademyEntity;

    // @ApiProperty()
    // readonly dataPastoral: DataPastoralEntity;
}

export class UpdateUserDto extends PartialType(UserCreateDto) {}