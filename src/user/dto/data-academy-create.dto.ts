import {
    IsNotEmpty
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from '../entities/user.entity';

export class DataAcademyCreateDto {
    @IsNotEmpty()
    @ApiProperty()
    readonly levelOfStudy: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly institutionName: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly titleReceived: string;

    @ApiProperty()
    readonly year: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly lastYearStudied: string;

    @ApiProperty()
    readonly culminationDate: Date;

    @ApiProperty()
    readonly projectedGradeDate: Date;

    @IsNotEmpty()
    @ApiProperty()
    readonly state: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly numberOfHours: number;

    @ApiProperty()
    readonly country: string;

    @ApiProperty()
    readonly department: string;

    @ApiProperty()
    readonly city: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly user: UserEntity;
}

export class UpdateDataAcademyDto extends PartialType(DataAcademyCreateDto) {}