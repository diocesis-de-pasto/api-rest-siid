import {
    IsNotEmpty
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from '../entities/user.entity';

export class DataPastoralCreateDto {
    @IsNotEmpty()
    @ApiProperty()
    readonly pastoralService: string;
    
    @IsNotEmpty()
    @ApiProperty()
    readonly assignedPlace: string;

    @ApiProperty()
    readonly appointmentDate: Date;

    @ApiProperty()
    readonly decreeNumber: string;

    @ApiProperty()
    readonly canonicalDecreeDate: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly serviceStartDate: Date;

    @ApiProperty()
    readonly serviceEndDate: Date;

    @IsNotEmpty()
    @ApiProperty()
    readonly user: UserEntity;
}

export class UpdateDataPastoralDto extends PartialType(DataPastoralCreateDto) {}