import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SendEmail } from 'src/email/sendEmail';
import { InstitutionEntity } from 'src/institution/entities/institution.entity';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { Repository } from 'typeorm';
import { UpdateUserDto, UserCreateDto } from '../dto/user-create.dto';
import { UserEntity } from '../entities/user.entity';
import { v4 as uuidv4 } from 'uuid';
import { hash } from 'bcryptjs';
import { QrService } from 'src/qr/services/qr.service';
import { RolesEntity } from 'src/roles/entities/roles.entity';
import { plainToClass } from 'class-transformer';
var shortid = require('shortid');

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(InstitutionEntity)
        private readonly institutionRepository: Repository<InstitutionEntity>,
        @InjectRepository(TypesUserEntity)
        private readonly typeUserRepository: Repository<TypesUserEntity>,
        private readonly qrService: QrService,
    ) { }

    async getOne(idUser: number) {
        const { institution, typeUser, role, ...user } = await this.userRepository.findOne({ idUser },{
            relations: ['institution', 'typeUser', 'role']
        });
        const userData = {
            "idUser": user.idUser,
            "documentId": user.documentId,
            "documentType": user.documentType,
            "email": user.email,
            "emailCorporate": user.emailCorporate,
            "state": user.state,
            "uuidQR": user.uuidQR,
            "numberOfAdmissions": user.numberOfAdmissions,
            "idInstitution": institution.idInstitution,
            "nameInstitution": institution.nameInstitution,
            "idTypeUser": typeUser.idTypesUser,
            "nameTypeUser": typeUser.nameTypeUser,
            "idRole": role.idRole,
            "nameRole": role.nameRole
        }
        return userData;
    }

    async getAll() {
        const users = await this.userRepository.find({
            relations: ['institution', 'typeUser', 'dataPerson','role']
        }).then(async res => {
            var usersAux = [];
            for (let i = 0; i < res.length; i++) {
                const dataQR = await this.qrService.generateQR(res[i].idUser);
                var names = '';
                var surnames = '';
                if(res[i].dataPerson){
                    names = res[i].dataPerson.firstName;
                    surnames = res[i].dataPerson.firstSurname;
                    names += res[i].dataPerson.secondName?(" "+res[i].dataPerson.secondName):" ";
                    surnames += res[i].dataPerson.secondSurname?(" "+res[i].dataPerson.secondSurname):"";
                }
                usersAux.push({
                    "nameInstitution": res[i].institution.nameInstitution,
                    "idInstitution": res[i].institution.idInstitution,
                    "nameTypeUser": res[i].typeUser.nameTypeUser,
                    "idTypesUser": res[i].typeUser.idTypesUser,
                    "idUser": res[i].idUser,
                    "documentId": res[i].documentId,
                    "email": res[i].email,
                    "emailCorporate": res[i].emailCorporate,
                    "state": res[i].state,
                    "qr": dataQR.qr,
                    "names": names,
                    "surnames": surnames,
                    "idRol": res[i].role.idRole,
                    "nameRole": res[i].role.nameRole
                })
            }
            return usersAux;
        })
        return users;
    }

    async findOneEmail(email: string) {
        return await this.userRepository.findOne({ email });
    }

    async create(data: UserCreateDto) {
        const institution = await this.institutionRepository.findOne(data.institution);
        const typeUser = await this.typeUserRepository.findOne(data.typeUser);
        const pass = await shortid.generate();
        var uuidQR = uuidv4();

        while (await this.userRepository.findOne({ uuidQR }) !== undefined) {
            uuidQR = uuidv4();
        }

        if (institution) {
            const newUser = new UserEntity();
            newUser.email = data.email;
            newUser.documentId = data.documentId;
            newUser.documentType = data.documentType;
            newUser.institution = institution;
            newUser.typeUser = typeUser;
            newUser.password = pass;
            newUser.state = 'enable';
            newUser.uuidQR = uuidQR;
            newUser.numberOfAdmissions = 0;
            const role = new RolesEntity();
            role.idRole = 2,
                newUser.role = role;
            return await this.userRepository.save(newUser).then(res => {
                const jsonData = {
                    email: data.email,
                    password: pass
                }
                SendEmail('AccountCreation', jsonData);
                return res;
            })
        }
    }

    async update(email: string, data: UpdateUserDto, admin = false) {
        console.log("data ",data);
        console.log("admin ",admin);
        
        
        var updateUser = await this.userRepository.findOne({ email });
        const copyPass = updateUser.password;
        const copyEmail = updateUser.email;
        const copyNumbAdm = updateUser.numberOfAdmissions;
        const copyTypeUser = updateUser.typeUser;
        const copyRole = updateUser.role;
        const copyState = updateUser.state;

        this.userRepository.merge(updateUser, plainToClass(UserEntity,data));

        if (admin || copyNumbAdm === 0) {
            updateUser.numberOfAdmissions = 1;
        } else {
            updateUser.numberOfAdmissions = copyNumbAdm;
        }

        if (data.password && copyNumbAdm === 0) {
            updateUser.password = await hash(data.password, 10);
        } else {
            updateUser.password = copyPass;
        }

        if (admin && data.email) {
            updateUser.email = data.email;
        } else {
            updateUser.email = copyEmail;
        }

        if (admin && data.typeUser) {
            const typeUserExist:any = await this.typeUserRepository.findOne(plainToClass(TypesUserEntity,data.typeUser));
            if (typeUserExist) {
                updateUser.typeUser = typeUserExist;
            } else {
                updateUser.typeUser = copyTypeUser;
            }
        } else {
            updateUser.typeUser = copyTypeUser;
        }
        if (!admin && data.role) {
            updateUser.role = copyRole;
        }
        if (!admin && data.state) {
            updateUser.state = copyState;
        }
        return await this.userRepository.save(updateUser);
    }

    async delete(idUser: number) {
        return await this.userRepository.delete(idUser);
    }
}
