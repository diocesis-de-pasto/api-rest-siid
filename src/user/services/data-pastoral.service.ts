import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DataPastoralCreateDto, UpdateDataPastoralDto } from '../dto/data-pastoral.dto';
import { DataPastoralEntity } from '../entities/data-pastoral.entity';
import { UserEntity } from '../entities/user.entity';

@Injectable()
export class DataPastoralService {

    constructor(
        @InjectRepository(DataPastoralEntity)
        private readonly dataPastoralRepository: Repository<DataPastoralEntity>
    ) { }

    async getDataPastoral(userEntity: UserEntity) {
        const dataPastoral = await this.dataPastoralRepository.find({ user: userEntity });
        return dataPastoral;
    }

    async create(data: DataPastoralCreateDto) {
        const dataPastoral = await this.dataPastoralRepository.create(data);
        console.log("dataPastoral ",dataPastoral);
        
        const dataPastoralSave = await this.dataPastoralRepository.save(dataPastoral);
        return dataPastoralSave;
    }

    async update(idDataPastoral: number, dataUpdate: UpdateDataPastoralDto) {
        const dataAcademyUpdate = await this.dataPastoralRepository.findOne(idDataPastoral);
        this.dataPastoralRepository.merge(dataAcademyUpdate, dataUpdate);
        const { user, ...dataPersonReady } = dataAcademyUpdate;
        return await this.dataPastoralRepository.save(dataPersonReady);
    }

    async delete(idDataPastoral: number) {
        return await this.dataPastoralRepository.delete(idDataPastoral);
    }
}
