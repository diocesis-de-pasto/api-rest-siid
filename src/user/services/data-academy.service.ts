import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DataAcademyCreateDto, UpdateDataAcademyDto } from '../dto/data-academy-create.dto';
import { DataAcademyEntity } from '../entities/data-academy.entity';
import { UserEntity } from '../entities/user.entity';

@Injectable()
export class DataAcademyService {

    constructor(
        @InjectRepository(DataAcademyEntity)
        private readonly dataAcademyRepository: Repository<DataAcademyEntity>
    ) { }

    async getDataAcademy(userEntity: UserEntity) {
        const dataAcademy = await this.dataAcademyRepository.find({ user: userEntity });
        return dataAcademy;
    }

    async create(data: DataAcademyCreateDto) {
        const dataAcademy = await this.dataAcademyRepository.create(data);
        const dataAcademySave = await this.dataAcademyRepository.save(dataAcademy);
        return dataAcademySave;
    }

    async update(idDataAcademy: number, dataUpdate: UpdateDataAcademyDto) {
        const dataAcademyUpdate = await this.dataAcademyRepository.findOne(idDataAcademy);
        this.dataAcademyRepository.merge(dataAcademyUpdate, dataUpdate);
        const { user, ...dataPersonReady } = dataAcademyUpdate;
        return await this.dataAcademyRepository.save(dataPersonReady);
    }

    async delete(idDataAcademy: number) {
        return await this.dataAcademyRepository.delete(idDataAcademy);
    }
}
