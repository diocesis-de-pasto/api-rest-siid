import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DataPersonCreateDto, UpdateDataPersonDto } from '../dto/data-person-create.dto';
import { DataPersonEntity } from '../entities/data-person.entity';
import { UserEntity } from '../entities/user.entity';

@Injectable()
export class DataPersonService {

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(DataPersonEntity)
        private readonly dataPersonRepository: Repository<DataPersonEntity>
    ) { }

    async getDataPerson(userEntity: UserEntity) {
        const dataPerson = await this.dataPersonRepository.findOne({ user: userEntity }, {
            relations: ['user']
        });
        try {
            const { user, ...data } = dataPerson;
            return data;
        } catch (error) {
            return dataPerson;
        }
    }

    async create(data: DataPersonCreateDto, user: UserEntity) {
        const userUpdate = await this.userRepository.findOne(user);
        const dataPerson = await this.dataPersonRepository.create(data);
        const dataPersonSave = await this.dataPersonRepository.save(dataPerson);
        userUpdate.dataPerson = dataPersonSave;
        dataPerson.photo = "Photo";
        this.userRepository.merge(userUpdate, user);
        this.userRepository.save(userUpdate);
        return dataPersonSave;
    }

    async updateImage(idUser: number, nameFile: string) {
        const dataUpdate = new DataPersonEntity();
        const updateUser = await this.userRepository.findOne(idUser, {
            relations: ['dataPerson']
        });
        const dataPersonUpdate = await this.dataPersonRepository.findOne(updateUser.dataPerson);
        dataUpdate.photo = nameFile;
        this.dataPersonRepository.merge(dataPersonUpdate, dataUpdate);
        const { user, ...dataPersonReady } = dataPersonUpdate;
        return await this.dataPersonRepository.save(dataPersonReady);
    }

    async update(idUser: number, dataUpdate: UpdateDataPersonDto) {
        const updateUser = await this.userRepository.findOne(idUser, {
            relations: ['dataPerson']
        });
        const copyPhoto = updateUser.dataPerson.photo;
        const dataPersonUpdate = await this.dataPersonRepository.findOne(updateUser.dataPerson);
        this.dataPersonRepository.merge(dataPersonUpdate, dataUpdate);
        if(dataUpdate.photo && dataUpdate.photo==='null'){
            dataPersonUpdate.photo = 'Photo';
        }else{
            dataPersonUpdate.photo = copyPhoto;
        }
        const { user, ...dataPersonReady } = dataPersonUpdate;
        return await this.dataPersonRepository.save(dataPersonReady);
    }

    async delete(idUser: number) {
        const user = await this.userRepository.findOne(idUser,{
            relations: ['dataPerson']
        });
        const auxDataPerson = user.dataPerson;
        user.dataPerson = null;
        await this.userRepository.save(user);
        return await this.dataPersonRepository.delete(auxDataPerson);
    }
}
