import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";

@Entity('data_person')
export class DataPersonEntity {
    @PrimaryGeneratedColumn()
    idDataPerson: number;

    @Column({nullable:true})
    expeditionDocumentDate: Date;

    @Column({nullable:true})
    expeditionDocumentPlace: string;

    @Column()
    firstName: string;

    @Column({nullable: true})
    secondName: string;

    @Column()
    firstSurname: string;

    @Column({nullable: true})
    secondSurname: string;

    @Column()
    dateOfBirth: Date;

    @Column({nullable: true})
    cityOfBirth: string;

    @Column({nullable:true})
    departmentOfBirth: string;

    @Column({nullable:true})
    countryOfBirth: string;

    @Column({nullable:true})
    gender: string;

    @Column({nullable: true})
    photo: string;

    @Column({nullable:true})
    residenceAddress: string;

    @Column()
    bloodType: string;

    @Column()
    contactNumberOne: string;

    @Column({nullable: true})
    contactNumberTwo: string;

    @OneToOne(() => UserEntity, (user) => user.dataPerson)
    user: UserEntity;
}
