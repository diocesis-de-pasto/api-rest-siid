import { BeforeInsert, Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { hash } from "bcryptjs";
import { InstitutionEntity } from "src/institution/entities/institution.entity";
import { DataPersonEntity } from "./data-person.entity";
import { DataAcademyEntity } from "./data-academy.entity";
import { TypesUserEntity } from "src/types-user/entities/types-user.entity";
import { MinisterialInformationEntity } from "src/ministerial-information/entities/ministerial-information.entity";
import { DataPastoralEntity } from "./data-pastoral.entity";
import { RecognitionEntity } from "src/recognitions/entities/recognitions.entity";
import { FamilyContactEntity } from "src/family-contact/entities/family-contact.entity";
import { RolesEntity } from "src/roles/entities/roles.entity";

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  idUser: number;

  @Column({ unique: true })
  documentId: string;

  @Column()
  documentType: string;

  @Column({ unique: true })
  email: string;

  @Column({ nullable: true })
  emailCorporate: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  state: string;

  @Column({ nullable: true })
  uuidQR: string;

  @Column()
  numberOfAdmissions: number;

  @ManyToOne(() => InstitutionEntity, institution => institution.user, {nullable: false})
  @JoinColumn()
  institution: InstitutionEntity;

  @ManyToOne(() => TypesUserEntity, (typeUser) => typeUser.user, {nullable: false})
  @JoinColumn()
  typeUser: TypesUserEntity;

  @OneToOne(() => DataPersonEntity, (dataPerson) => dataPerson.user, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  dataPerson: DataPersonEntity;

  @OneToOne(() => MinisterialInformationEntity, (dataMinisterial) => dataMinisterial.user, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  dataMinisterial: MinisterialInformationEntity;

  @OneToOne(() => FamilyContactEntity, (famCont) => famCont.user, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  @JoinColumn()
  familyContact: FamilyContactEntity;

  @OneToMany(() => DataAcademyEntity, (dataAc) => dataAc.user, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  dataAcademy: DataAcademyEntity[];

  @OneToMany(() => DataPastoralEntity, (dataPastoral) => dataPastoral.user, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  dataPastoral: DataPastoralEntity[];

  @OneToMany(() => RecognitionEntity, (rec) => rec.user, {
    nullable: true,
    onDelete: 'CASCADE'
  })
  recognitions: RecognitionEntity[];

  @ManyToOne(() => RolesEntity, (role) => role.idRole, {nullable: true})
  role: RolesEntity;

  @BeforeInsert()
  async hashPassword() {
    if (!this.password) {
      return;
    }
    this.password = await hash(this.password, 10);
  }
}
