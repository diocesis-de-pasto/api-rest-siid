import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";

@Entity('data_academy')
export class DataAcademyEntity {
    @PrimaryGeneratedColumn()
    idDataAcademy: number;

    @Column()
    levelOfStudy: string;

    @Column()
    institutionName: string;

    @Column()
    titleReceived: string;

    @Column({nullable:true})
    year: string;

    @Column({nullable:true})
    lastYearStudied: string;

    @Column({nullable:true})
    culminationDate: Date;

    @Column({nullable:true})
    projectedGradeDate: Date;

    @Column()
    state: string;

    @Column()
    numberOfHours: number;

    @Column({nullable:true})
    country: string;

    @Column({nullable:true})
    department: string;

    @Column({nullable:true})
    city: string;

    @ManyToOne(()=>UserEntity,(user)=>user.dataAcademy,{
        onDelete: 'CASCADE'
    })
    user: UserEntity;
}
