import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";


@Entity('data_pastoral')
export class DataPastoralEntity {
    @PrimaryGeneratedColumn()
    idDataPastoral: number;

    @Column()
    pastoralService: string;

    @Column()
    assignedPlace: string;

    @Column({nullable:true})
    appointmentDate: Date;

    @Column({nullable:true})
    decreeNumber: string;

    @Column({nullable:true})
    canonicalDecreeDate: string;

    @Column({nullable:true})
    serviceStartDate: Date;

    @Column({nullable: true})
    serviceEndDate: Date;

    @ManyToOne(()=>UserEntity,(user)=>user.dataPastoral,{
        onDelete: 'CASCADE'
    })
    user: UserEntity;
}
