import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstitutionEntity } from 'src/institution/entities/institution.entity';
import { InstitutionService } from 'src/institution/services/institution.service';
import { UserEntity } from './entities/user.entity';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { DataAcademyEntity } from './entities/data-academy.entity';
import { DataPersonEntity } from './entities/data-person.entity';
import { DataPersonController } from './controllers/data-person.controller';
import { DataPersonService } from './services/data-person.service';
import { DataAcademyController } from './controllers/data-academy.controller';
import { DataAcademyService } from './services/data-academy.service';
import { DataPastoralEntity } from './entities/data-pastoral.entity';
import { DataPastoralService } from './services/data-pastoral.service';
import { DataPastoralController } from './controllers/data-pastoral.controller';
import { QrService } from 'src/qr/services/qr.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            UserEntity, 
            InstitutionEntity, 
            AuditHandlerEntity, 
            LogExceptionHandlerEntity,
            TypesUserEntity,
            DataPersonEntity,
            DataAcademyEntity,
            DataPastoralEntity
        ]),
    ],
    providers: [
        UserService, 
        InstitutionService, 
        AuditHandlerService, 
        LogExceptionHandlerService,
        DataPersonService,
        DataAcademyService,
        DataPastoralService,
        QrService
    ],
    controllers: [
        UserController,
        DataPersonController,
        DataAcademyController,
        DataPastoralController
    ],
    exports: [UserService]
})
export class UserModule { }
