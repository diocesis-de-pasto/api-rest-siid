export const renameImage = (req, file, callback)=>{
    var fileName;
    try {
        fileName = file.originalname;
    } catch (error) {
        fileName = '';
    }
    const time = new Date().getTime();
    callback(null, `${time}-${fileName}`)
}

export const fileFilter = (req, file, callback)=>{
    if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return callback(new Error('Formato inválido'), false)
    }
    callback(null, true);
}