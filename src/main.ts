import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const config = new DocumentBuilder()
    .setTitle('Diócesis de Pasto')
    .setDescription('Documentación API')
    .setVersion('1.0')
    .addBearerAuth(
      {type:'http', scheme: 'bearer', bearerFormat: 'JWT', in: 'header'},
      'Authorization'
    )
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document, {
    swaggerOptions: {
      filter: true,
      showRequestDuration: true,
      defaultModelsExpandDepth: -1,
      docExpansion: "none"
    }
  });

  await app.listen(3000);
}
bootstrap();
