import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { InstitutionModule } from './institution/institution.module';
import { AuthModule } from './auth/auth.module';
import configuration from './config/configuration';
import { LoginModule } from './login/login.module';
import { AuditHandlerModule } from './audit-handler/audit-handler.module';
import { LogExceptionHandlerModule } from './log-exception-handler/log-exception-handler.module';
import { RolesModule } from './roles/roles.module';
import { ModuleModule } from './module/module.module';
import { RegistroCedulaEmailModule } from './registro-cedula-email/registro-cedula-email.module';
import { MinisterialInformationModule } from './ministerial-information/ministerial-information.module';
import { RecognitionsModule } from './recognitions/recognitions.module';
import { FamilyContactModule } from './family-contact/family-contact.module';
import { QrModule } from './qr/qr.module';
import { RecoveryPasswordModule } from './recovery-password/recovery-password.module';
import { PermissionModule } from './permission/permission.module';
import { CarnetModule } from './carnet/carnet.module';
import { TypesUserModule } from './types-user/types-user.module';
import { StatisticsModule } from './statistics/statistics.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
      envFilePath: ['.env']
    }),
    TypeOrmModule.forRootAsync({
      inject:[ConfigService],
      useFactory: (config:ConfigService) => ({
        type: 'postgres',
        host: config.get<string>('database.host'),
        port: parseInt(config.get<string>('database.port'),10),
        username: config.get<string>('database.username'),
        password: config.get<string>('database.password'),
        database: config.get<string>('database.namedb'),
        entities: ["dist/**/*.entity{.ts,.js}"],
        // entities: [config.get<string>('database.entities')],
        autoLoadEntities: true,
        synchronize: config.get<boolean>('database.synchronize'),
        logging: config.get<boolean>('database.logging'),
        logger: 'file',
        migrationsTableName: config.get<string>('typeORM.migrationsTableName'),
        // migrations: [config.get<string>('typeORM.migrations')],
        cli: {
          "migrationsDir": config.get<string>('typeORM.migrationsDir')
        }
      })
    }),
    UserModule,
    InstitutionModule,
    AuthModule,
    LoginModule,
    AuditHandlerModule,
    LogExceptionHandlerModule,
    RolesModule,
    ModuleModule,
    RegistroCedulaEmailModule,
    MinisterialInformationModule,
    RecognitionsModule,
    FamilyContactModule,
    QrModule,
    RecoveryPasswordModule,
    PermissionModule,
    CarnetModule,
    TypesUserModule,
    StatisticsModule
  ],
  controllers: [
    AppController,
  ],
  providers: [
    AppService
  ],
})
export class AppModule {}
