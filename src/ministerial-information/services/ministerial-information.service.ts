import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { MinisterialInformationCreateDto, UpdateMinisterialInformationDto } from '../dtos/ministerial-information-create.dto';
import { MinisterialInformationEntity } from '../entities/ministerial-information.entity';

@Injectable()
export class MinisterialInformationService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(MinisterialInformationEntity)
        private readonly ministerialInformationRepository: Repository<MinisterialInformationEntity>
    ) { }

    async getOne(userEntity: UserEntity) {
        const {user,...dataMinisterial} = await this.ministerialInformationRepository.findOne({user: userEntity},{
            relations: ['user']
        });
        
        return dataMinisterial;
    }

    async create(data: MinisterialInformationCreateDto, user: UserEntity) {
        const userUpdate = await this.userRepository.findOne(user);
        const ministerialInformation = await this.ministerialInformationRepository.create(data);
        const dataMinisterialSave = await this.ministerialInformationRepository.save(ministerialInformation);
        userUpdate.dataMinisterial = dataMinisterialSave;
        this.userRepository.merge(userUpdate, user);
        this.userRepository.save(userUpdate);
        return dataMinisterialSave;
    }

    async update(idUser: number, dataUpdate: UpdateMinisterialInformationDto) {
        const updateUser = await this.userRepository.findOne({idUser}, {
            relations: ['dataMinisterial']
        });
        const dataMinisterialUpdate = await this.ministerialInformationRepository.findOne(updateUser.dataMinisterial);
        this.ministerialInformationRepository.merge(dataMinisterialUpdate, dataUpdate);
        const { user, ...dataMinistaialReady } = dataMinisterialUpdate;
        return await this.ministerialInformationRepository.save(dataMinistaialReady);
    }

    async delete(idUser: number) {
        const user = await this.userRepository.findOne(idUser,{
            relations: ['dataMinisterial']
        })
        const auxDataMinisterial = user.dataMinisterial;
        user.dataMinisterial = null;
        await this.userRepository.save(user);
        return await this.ministerialInformationRepository.delete(auxDataMinisterial);
    }
}
