import { Module } from '@nestjs/common';
import { MinisterialInformationService } from './services/ministerial-information.service';
import { MinisterialInformationController } from './controllers/ministerial-information.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { MinisterialInformationEntity } from './entities/ministerial-information.entity';
import { UserService } from 'src/user/services/user.service';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { DataAcademyEntity } from 'src/user/entities/data-academy.entity';
import { DataPersonEntity } from 'src/user/entities/data-person.entity';
import { InstitutionEntity } from 'src/institution/entities/institution.entity';
import { DataPastoralEntity } from 'src/user/entities/data-pastoral.entity';
import { QrService } from 'src/qr/services/qr.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
        UserEntity,
        InstitutionEntity,
        TypesUserEntity,
        DataPersonEntity,
        DataAcademyEntity,
        DataPastoralEntity,
        AuditHandlerEntity, 
        LogExceptionHandlerEntity,
        MinisterialInformationEntity
    ]),
],
  providers: [
    MinisterialInformationService,
    UserService,
    AuditHandlerService, 
    LogExceptionHandlerService,
    QrService
  ],
  controllers: [MinisterialInformationController]
})
export class MinisterialInformationModule {}
