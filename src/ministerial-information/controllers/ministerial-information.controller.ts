import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { MinisterialInformationCreateDto, UpdateMinisterialInformationDto } from '../dtos/ministerial-information-create.dto';
import { MinisterialInformationService } from '../services/ministerial-information.service';

@ApiTags('ministerial-information')
@Controller('ministerial-information')
export class MinisterialInformationController {
    constructor(
        private readonly ministerialInfService: MinisterialInformationService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar informacion ministerial')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const ministerialInfoFind = await this.ministerialInfService.getOne(user)
        .catch(err=>this.logExcHandler.create(req, err));
        if (ministerialInfoFind) {
            if(req.user.idUser!==userId){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return ministerialInfoFind;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'La información ministerial del usuario no se ha podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear informacion ministerial')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: MinisterialInformationCreateDto) {
        const user = new UserEntity();
        user.idUser = data.user.idUser;
        const ministerialInfoFind = await this.ministerialInfService.getOne(user)
        .catch(err=>this.logExcHandler.create(req, err));
        if (!ministerialInfoFind) {
            if(req.user.idUser!==data.user.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const ministerialInfoNew = await this.ministerialInfService.create(data,user)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo crear la información ministerial del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'DataMinisterialEntity', JSON.stringify(null), JSON.stringify(ministerialInfoNew));
            return new HttpException('La información ministerial del usuario se creó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'La información ministerial del usuario ya está registrada',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar informacion ministerial')
    @Put()
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateMinisterialInformationDto) {
        const user = new UserEntity();
        user.idUser = data.user.idUser;
        const ministerialInfoFind = await this.ministerialInfService.getOne(user)
        .catch(err=>this.logExcHandler.create(req, err));
        if (ministerialInfoFind) {
            if(req.user.idUser!==data.user.idUser){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const ministerialInfoNew = await this.ministerialInfService.update(user.idUser, data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo actualizar la información ministerial del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'DataMinisterialEntity', JSON.stringify(ministerialInfoFind), JSON.stringify(ministerialInfoNew));
            return new HttpException('La información ministerial del usuario se actualizó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'La información ministerial del usuario no se encuentra registrada',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar informacion ministerial')
    @Delete(':idUser')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idUser') idUser: number) {
        const user = new UserEntity();
        user.idUser = idUser;
        const ministerialInfoFind = await this.ministerialInfService.getOne(user)
        .catch(err=>this.logExcHandler.create(req, err));
        if (ministerialInfoFind) {
            if (req.user.idUser !== user.idUser) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const ministerialInfoDelete = await this.ministerialInfService.delete(idUser)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo eliminar la información ministerial del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'DataMinisterialEntity', JSON.stringify(ministerialInfoFind), JSON.stringify(null));
            return new HttpException('La información ministerial del usuario se eliminó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'La información ministerial del usuario no se encuentra registrada',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
