import { UserEntity } from "src/user/entities/user.entity";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('ministerial_information')
export class MinisterialInformationEntity {
    @PrimaryGeneratedColumn()
    idMinisterialInformation: number;

    @Column()
    diaconalOrdinationDate: Date;
    
    @Column()
    priestlyOrdinationDate: Date;

    @Column({nullable: true})
    episcopalOrdinationDate: Date;

    @OneToOne(() => UserEntity, (user) => user.dataMinisterial)
    user: UserEntity;
}
