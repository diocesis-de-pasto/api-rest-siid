import {
    IsNotEmpty,
    IsEmail,
    MinLength,
    IsDate,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from 'src/user/entities/user.entity';

export class MinisterialInformationCreateDto {
    @IsNotEmpty()
    @IsDate()
    @ApiProperty()
    readonly diaconalOrdinationDate: Date;
    
    @IsNotEmpty()
    @IsDate()
    @ApiProperty()
    readonly priestlyOrdinationDate: Date;
    
    @IsDate()
    @ApiProperty()
    readonly episcopalOrdinationDate: Date;

    @IsNotEmpty()
    @ApiProperty()
    readonly user: UserEntity;
}

export class UpdateMinisterialInformationDto extends PartialType(MinisterialInformationCreateDto) {}