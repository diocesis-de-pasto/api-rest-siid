import { Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { QrService } from '../services/qr.service';

@ApiTags('qr')
@Controller('qr')
export class QrController {
    constructor(
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService,
        private readonly qrService: QrService
    ) { }

    @UseGuards(JwtAuthGuard)
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Obtener código qr')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async getQR(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const qr = await this.qrService.generateQR(userId)
            .catch(err => this.logExcHandler.create(req, err));            
        if (qr) {
            if(req.user.idUser!==userId){
                if(!req.admin){
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return qr;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no existe',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @Get('dataQR/:codeQr')
    async get(@Req() req, @Param('codeQr') codeQr: string) {
        const user = await this.qrService.getDataUser(codeQr)
        if(user) {
            return user;
        }else{
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no existe',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
