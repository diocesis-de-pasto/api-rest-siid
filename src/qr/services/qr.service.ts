import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
var QRCode = require('qrcode')
import * as fs from 'fs';

@Injectable()
export class QrService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
    ) { }

    async generateQR(idUser) {
        const user = await this.userRepository.findOne(idUser);
        if(user) {
            const qr = await QRCode.toDataURL(`${process.env.PATHDDP}qr/${user.uuidQR}`);
            return {
                qr
            }
        }else{
            return null;
        }
    }

    async getDataUser(qrCode: string) {
        const {institution,typeUser,dataPerson,dataPastoral,...basic} = await this.userRepository.findOne({uuidQR:qrCode},{
            relations: ['institution','typeUser','dataPerson','dataPastoral']
        })
        
        const userData = {};
        var documentNumber = ''; 
        for (let i = 0; i < basic.documentId.length; i++) {
            if(i>basic.documentId.length-3) {
                documentNumber = documentNumber + basic.documentId[i];
            }else{
                documentNumber = documentNumber + '*';
            }
        }
        var currentPastoral = dataPastoral.filter(dataPast=>dataPast.serviceEndDate===null||new Date(dataPast.serviceEndDate)>new Date());
        
        userData['documentId'] = documentNumber;
        userData['documentType'] = basic.documentType;
        userData['nameInstitution'] = institution?institution.nameInstitution:'';
        userData['nameTypeUser'] = typeUser.nameTypeUser;
        var image;
        if(dataPerson && dataPerson.photo && fs.existsSync(`./imagesUsers/${dataPerson.photo}`)){
            image = fs.createReadStream(`./imagesUsers/${dataPerson.photo}`);
            let buff = fs.readFileSync(`./imagesUsers/${dataPerson.photo}`);
            image = buff.toString('base64');
            image = `data:image/${dataPerson.photo.split('.')[1]};base64,${image}`;
        }else{
            image = ''
        }
        if(dataPerson){
            userData['nameUser'] = [dataPerson.firstName,dataPerson.secondName,dataPerson.firstSurname,dataPerson.secondSurname].join(' ');
            userData['photo'] = image;
        }else{
            userData['nameUser'] = '';
            userData['photo'] = image;
        }
        if(currentPastoral.length>0){
            currentPastoral.forEach(dataPast=>{
                if(dataPast.serviceEndDate!==null){
                    const remainingDays = (new Date(dataPast.serviceEndDate).getTime() - new Date().getTime())/(1000*60*60*24);
                    dataPast["timeRemaining"] = Math.round(remainingDays);
                    delete dataPast["decreeNumber"];
                    delete dataPast["canonicalDecreeDate"];
                }else{
                    dataPast["timeRemaining"] = -1;
                }
            })
            userData['dataPastoral'] = currentPastoral;
        }        
        return userData;
    }
}
