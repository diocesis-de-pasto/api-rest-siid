import { Module } from '@nestjs/common';
import { QrService } from './services/qr.service';
import { QrController } from './controllers/qr.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
        UserEntity, 
        AuditHandlerEntity, 
        LogExceptionHandlerEntity,
    ]),
],
  providers: [
    QrService,
    AuditHandlerService, 
    LogExceptionHandlerService,
  ],
  controllers: [QrController],
  exports: [
    QrService
  ]
})
export class QrModule {}
