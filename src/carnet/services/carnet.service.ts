import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QrService } from 'src/qr/services/qr.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CarnetService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private qrService: QrService
        ) { }

    async getDataUser(userId: number) {
        const dataUser = await this.userRepository.findOne(userId,{
            relations: ['dataPerson','typeUser']
        });
        const qr = await this.qrService.generateQR(dataUser.idUser);
        return {dataUser,qr};
    }
}
