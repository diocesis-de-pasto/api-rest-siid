import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QrService } from 'src/qr/services/qr.service';
import { DataPersonEntity } from 'src/user/entities/data-person.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { CarnetController } from './controllers/carnet.controller';
import { CarnetService } from './services/carnet.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      DataPersonEntity
    ]),
  ],
  controllers: [CarnetController],
  providers: [CarnetService,QrService]
})
export class CarnetModule {}
