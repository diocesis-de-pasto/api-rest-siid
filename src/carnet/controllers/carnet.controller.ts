import { Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { CarnetService } from '../services/carnet.service';
import { carnet } from '../templates/carnet';   
import * as fs from 'fs';
import * as puppeteer from 'puppeteer';

@Controller('carnet')
export class CarnetController {

    constructor(private readonly carnetServices:CarnetService){}

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear carnet')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async getHello(@Res() res, @Req() req,@Param('userId', ParseIntPipe) userId: number) {
        if(req.user.idUser!==userId){
            if(!req.admin){
                throw new HttpException({
                    status: HttpStatus.FORBIDDEN,
                    error: 'No tiene los permisos suficientes',
                }, HttpStatus.FORBIDDEN);
            }
        }
        let options = {
            // format: 'a4',
            width: "5.5cm",
            height: "8.5cm",
            printBackground: true,
            margin: {
                left: '0px',
                top: '0px',
                right: '0px',
                bottom: '0px'
            }
        };
        const {dataUser,qr} = await this.carnetServices.getDataUser(userId);
        var names = "";
        var surnames = "";
        if (dataUser.dataPerson) {
            names = dataUser.dataPerson.firstName+" ";
            names+= dataUser.dataPerson.secondName?(dataUser.dataPerson.secondName+" "):'';
            surnames = dataUser.dataPerson.firstSurname+" ";
            surnames+= dataUser.dataPerson.secondSurname?dataUser.dataPerson.secondSurname:'';
        }
        const identification = dataUser.documentType+" "+dataUser.documentId;
        
        var image;
        if(dataUser.dataPerson && fs.existsSync(`./imagesUsers/${dataUser.dataPerson.photo}`)){
            image = fs.createReadStream(`./imagesUsers/${dataUser.dataPerson.photo}`);
            let buff = fs.readFileSync(`./imagesUsers/${dataUser.dataPerson.photo}`);
            image = buff.toString('base64');
            image = `data:image/${dataUser.dataPerson.photo.split('.')[1]};base64,${image}`;
        }else{
            image = '';
        }
        if(image !== '' && dataUser.dataPerson && names!=='' && surnames!==''){
            const browser = await puppeteer.launch({
                executablePath: process.env.CHROMIUM_PATH,
                args:['--no-sandbox']
            })
            const typeUserName = dataUser.typeUser.nameTypeUser.toUpperCase();
            const page = await browser.newPage()
            await page.setContent(carnet(names,surnames,typeUserName,identification,qr.qr,image))
            const buffer = await page.pdf(options)
            res.setHeader('Content-Type', 'application/pdf');
            res.end(buffer);
        }else{
            throw new HttpException({
                status: HttpStatus.CONFLICT,
                error: 'No se puede generar el carnet del usuario, falta información importante.',
            }, HttpStatus.CONFLICT);
        }
    }
}
