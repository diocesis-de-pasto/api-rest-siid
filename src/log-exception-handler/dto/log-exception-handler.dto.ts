import {
    IsString,
    IsNumber,
    IsNotEmpty,
    IsPositive,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LogExceptionHandlerDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly ipClient: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly browserData: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly endpoint: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly actionPerformed: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly errorData: string;
    
    @IsNumber()
    @IsPositive()
    @IsNotEmpty()
    @ApiProperty()
    readonly userId: number;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly userEmail: string;
}