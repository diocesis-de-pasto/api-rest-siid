import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LogExceptionHandlerEntity } from './entities/log-exception-handler.entity';
import { LogExceptionHandlerService } from './services/log-exception-handler.service';

@Module({
  imports: [TypeOrmModule.forFeature([LogExceptionHandlerEntity])],
  providers: [LogExceptionHandlerService],
  exports: [LogExceptionHandlerService]
})
export class LogExceptionHandlerModule {}
