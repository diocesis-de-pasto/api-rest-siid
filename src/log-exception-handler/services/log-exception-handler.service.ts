import { Injectable, Req } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LogExceptionHandlerDto } from '../dto/log-exception-handler.dto';
import { LogExceptionHandlerEntity } from '../entities/log-exception-handler.entity';

@Injectable()
export class LogExceptionHandlerService {
    constructor(
        @InjectRepository(LogExceptionHandlerEntity)
        private readonly logExcRepository:Repository<LogExceptionHandlerEntity>,
    ) { }

    async create(@Req() req, err: string) {
        var ip = (req.headers['x-forwarded-for'] || '').split(',').pop().trim() ||
            req.socket.remoteAddress || '';
        
        var action = 'create';
        if (req.method === 'PUT') {
            action = 'update';
        } else if (req.method === 'DELETE') {
            action = 'delete';
        } else if (req.method === 'GET') {
            action = 'get';
        }

        const data:LogExceptionHandlerDto = {
            ipClient: ip,
            browserData: req.headers['user-agent'],
            endpoint: req.url,
            actionPerformed: action,
            errorData: err,
            userId: req.user.idUser,
            userEmail: req.user.email
        };
        await this.logExcRepository.save(data);
    }
}