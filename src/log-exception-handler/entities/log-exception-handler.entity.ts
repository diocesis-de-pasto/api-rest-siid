import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('logs')
export class LogExceptionHandlerEntity {
    @PrimaryGeneratedColumn()
    idLogException: number;

    @Column()
    ipClient: string;

    @Column()
    browserData: string;

    @Column()
    endpoint: string;

    @Column()
    actionPerformed: string;

    @Column()
    errorData: string;

    @Column()
    userId: number;

    @Column()
    userEmail: string;

    @CreateDateColumn({
        type: 'timestamptz',
        default: () => 'CURRENT_TIMESTAMP'
    })
    date: Date;
}
