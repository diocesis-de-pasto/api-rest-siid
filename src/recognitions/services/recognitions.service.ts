import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { RecognitionsDto, UpdateRecognitionsDto } from '../dtos/recognitions.dto';
import { RecognitionEntity } from '../entities/recognitions.entity';

@Injectable()
export class RecognitionsService {
    constructor(
        @InjectRepository(RecognitionEntity)
        private readonly recognitionRepository: Repository<RecognitionEntity>
    ) { }

    async getRecognitions(userEntity: UserEntity) {
        const recognitions = await this.recognitionRepository.find({ user: userEntity });
        return recognitions;
    }

    async create(data: RecognitionsDto) {
        const recognitionNew = await this.recognitionRepository.create(data);
        const recognitionSave = await this.recognitionRepository.save(recognitionNew);
        return recognitionSave;
    }

    async update(idRecognition: number, dataUpdate: UpdateRecognitionsDto) {
        const recognitionUpdate = await this.recognitionRepository.findOne(idRecognition);
        this.recognitionRepository.merge(recognitionUpdate, dataUpdate);
        const { user, ...recognitionReady } = recognitionUpdate;
        return await this.recognitionRepository.save(recognitionReady);
    }

    async delete(idRecognition: number) {
        return await this.recognitionRepository.delete(idRecognition);
    }
}
