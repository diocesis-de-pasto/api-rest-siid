import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { InstitutionEntity } from 'src/institution/entities/institution.entity';
import { InstitutionService } from 'src/institution/services/institution.service';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { QrService } from 'src/qr/services/qr.service';
import { TypesUserEntity } from 'src/types-user/entities/types-user.entity';
import { DataAcademyEntity } from 'src/user/entities/data-academy.entity';
import { DataPastoralEntity } from 'src/user/entities/data-pastoral.entity';
import { DataPersonEntity } from 'src/user/entities/data-person.entity';
import { UserEntity } from 'src/user/entities/user.entity';
import { DataAcademyService } from 'src/user/services/data-academy.service';
import { DataPastoralService } from 'src/user/services/data-pastoral.service';
import { DataPersonService } from 'src/user/services/data-person.service';
import { UserService } from 'src/user/services/user.service';
import { RecognitionsController } from './controllers/recognitions.controller';
import { RecognitionEntity } from './entities/recognitions.entity';
import { RecognitionsService } from './services/recognitions.service';

@Module({
    imports: [
      TypeOrmModule.forFeature([
        UserEntity, 
        InstitutionEntity,
        AuditHandlerEntity, 
        LogExceptionHandlerEntity,
        TypesUserEntity,
        DataPersonEntity,
        RecognitionEntity
      ]),
  ],
  providers: [
    RecognitionsService,
    UserService,
    AuditHandlerService, 
    LogExceptionHandlerService,
    DataPersonService,
    QrService
  ],
  controllers: [RecognitionsController]
})
export class RecognitionsModule {}
