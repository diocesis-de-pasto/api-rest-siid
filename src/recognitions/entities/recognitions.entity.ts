import { UserEntity } from "src/user/entities/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('recognition')
export class RecognitionEntity {
    @PrimaryGeneratedColumn()
    idRecognition: number;

    @Column()
    recognitionMotive: string;

    @Column()
    recognitionDate: Date;

    @Column()
    entityThatIssuesRecognition: string;

    @ManyToOne(()=>UserEntity,(user)=>user.recognitions,{
        onDelete: 'CASCADE'
    })
    user: UserEntity;
}
