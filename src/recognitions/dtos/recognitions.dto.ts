import {
    IsNotEmpty
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { UserEntity } from 'src/user/entities/user.entity';

export class RecognitionsDto {
    @IsNotEmpty()
    @ApiProperty()
    recognitionMotive: string;

    @IsNotEmpty()
    @ApiProperty()
    recognitionDate: Date;

    @IsNotEmpty()
    @ApiProperty()
    entityThatIssuesRecognition: string;

    @IsNotEmpty()
    @ApiProperty()
    readonly user: UserEntity;
}

export class UpdateRecognitionsDto extends PartialType(RecognitionsDto) {}