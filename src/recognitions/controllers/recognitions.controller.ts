import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';
import { RecognitionsDto, UpdateRecognitionsDto } from '../dtos/recognitions.dto';
import { RecognitionsService } from '../services/recognitions.service';

@ApiTags('recognitions')
@Controller('recognitions')
export class RecognitionsController {
    constructor(private readonly recognitionsService: RecognitionsService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService,
        private readonly userService: UserService,
    ) { }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar reconocimientos')
    @Get(':userId')
    @ApiBearerAuth('Authorization')
    async get(@Req() req, @Param('userId', ParseIntPipe) userId: number) {
        const user = new UserEntity();
        user.idUser = userId;
        const recognitions = await this.recognitionsService.getRecognitions(user)
            .catch(err => this.logExcHandler.create(req, err));
        if (recognitions) {
            if (req.user.idUser !== userId) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            return recognitions;
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'Los reconocimientos del usuario no se han podido encontrar',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear reconocimiento')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req, @Body() data: RecognitionsDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if (req.user.idUser !== userFind.idUser) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const recognitionNew = await this.recognitionsService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo guardar el nuevo reconocimiento del usuario',
                    }, HttpStatus.CONFLICT);
                })
            this.auditHandler.create(req, 'RecognitionEntity', JSON.stringify(null), JSON.stringify(recognitionNew));
            return new HttpException('El nuevo reconocimiento del usuario se guardó correctamente', HttpStatus.OK);
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no está registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar reconocimiento')
    @Put(':idRecognition')
    @ApiBearerAuth('Authorization')
    async update(@Req() req, @Body() data: UpdateRecognitionsDto, @Param('idRecognition', ParseIntPipe) idRecognition: number) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            if (req.user.idUser !== userFind.idUser) {
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const recongnitions = await this.recognitionsService.getRecognitions(user)
                .catch(err => this.logExcHandler.create(req, err));
            if (recongnitions && recongnitions.find(res => res.idRecognition === idRecognition)) {
                const recognitionsUpdate = await this.recognitionsService.update(idRecognition, data)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo actualizar el reconocimiento del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'RecognitionEntity', JSON.stringify(recongnitions), JSON.stringify(recognitionsUpdate));
                return new HttpException('El reconocimiento se actualizó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no ha creado los datos',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar reconocimiento')
    @Delete(':idRecognition')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req, @Param('idRecognition', ParseIntPipe) idRecognition: number, @Body() data: RecognitionsDto) {
        const userFind = await this.userService.getOne(data.user.idUser)
            .catch(err => this.logExcHandler.create(req, err));
        if (userFind) {
            if(userFind.idUser!==req.user.idUser){
                if (!req.admin) {
                    throw new HttpException({
                        status: HttpStatus.FORBIDDEN,
                        error: 'No tiene los permisos suficientes',
                    }, HttpStatus.FORBIDDEN);
                }
            }
            const user = new UserEntity();
            user.idUser = userFind.idUser;
            const recognitions = await this.recognitionsService.getRecognitions(user)
                .catch(err => this.logExcHandler.create(req, err))
            if (recognitions && recognitions.find(res => res.idRecognition === idRecognition)) {
                const recognitionDelete = await this.recognitionsService.delete(idRecognition)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar el reconocimiento del usuario',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'RecognitionEntity', JSON.stringify(recognitions), JSON.stringify(null));
                return new HttpException('El reconocimiento del usuario se eliminó correctamente', HttpStatus.OK);

            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'El usuario no tiene reconocimientos guardados',
                }, HttpStatus.NO_CONTENT);
            }
        } else {
            throw new HttpException({
                status: HttpStatus.NO_CONTENT,
                error: 'El usuario no se encuentra registrado',
            }, HttpStatus.NO_CONTENT);
        }
    }
}
