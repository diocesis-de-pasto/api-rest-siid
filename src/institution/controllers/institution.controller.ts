import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { InstitutionDto, UpdateInstitutionDto } from '../dto/institution.dto';
import { InstitutionService } from '../services/institution.service';

@ApiTags('institution')
@Controller('institution')
export class InstitutionController {
    constructor(
        private readonly institutionService:InstitutionService,
        private readonly auditHandler: AuditHandlerService,
        private readonly logExcHandler: LogExceptionHandlerService
    ){}

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Listar institución')
    @Get()
    @ApiBearerAuth('Authorization')
    async all(@Req() req){
        if(req.admin){
            return this.institutionService.getAllWithInstitution();
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Crear institución')
    @Post()
    @ApiBearerAuth('Authorization')
    async create(@Req() req,@Body() data: InstitutionDto){
        if(req.admin){
            const institucion = await this.institutionService.getOneByName(data.nameInstitution)
            .catch(err=>this.logExcHandler.create(req, err));            
            if(!institucion){
                const institution = await this.institutionService.create(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo crear la institución',
                    }, HttpStatus.CONFLICT);
                })
                this.auditHandler.create(req, 'InstitutionEntity', JSON.stringify(null), JSON.stringify(institution));
                return new HttpException('La institución se creó correctamente', HttpStatus.OK);
            }else{
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'Ya existe una institución con ese nombre',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Editar institución')
    @Put()
    @ApiBearerAuth('Authorization')
    async edit(@Req() req,@Body() data: UpdateInstitutionDto){
        if(req.admin){
            const institutionFind = await this.institutionService.getOne(data.idInstitution)
            .catch(err=>this.logExcHandler.create(req, err));
            if(institutionFind && data.idInstitution!==undefined){
                const institution = await this.institutionService.update(data)
                .catch(err => {
                    this.logExcHandler.create(req, err)
                    throw new HttpException({
                        status: HttpStatus.CONFLICT,
                        error: 'No se pudo actualizar la institución, es posible que el nombre esté relacionado con otra institución',
                    }, HttpStatus.CONFLICT);
                })
                this.auditHandler.create(req, 'InstitutionEntity', JSON.stringify(null), JSON.stringify(institution));
                return new HttpException('La institución se actualizó correctamente', HttpStatus.OK);
            }else{
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: `No existe la institución con el id ${data.idInstitution}`,
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('Eliminar institución')
    @Delete(':idInstitution')
    @ApiBearerAuth('Authorization')
    async delete(@Req() req,@Param('idInstitution') idInstitution: number){
        if(req.admin){
            const institutionFind = await this.institutionService.getOne(idInstitution)
            .catch(err=>this.logExcHandler.create(req, err));
            if (institutionFind) {
                const institutionDelete = await this.institutionService.delete(idInstitution)
                    .catch(err => {
                        this.logExcHandler.create(req, err)
                        throw new HttpException({
                            status: HttpStatus.CONFLICT,
                            error: 'No se pudo eliminar la institución',
                        }, HttpStatus.CONFLICT);
                    })
                this.auditHandler.create(req, 'UserEntity', JSON.stringify(institutionFind), JSON.stringify(null));
                return new HttpException('La institución se eliminó correctamente', HttpStatus.OK);
            } else {
                throw new HttpException({
                    status: HttpStatus.NO_CONTENT,
                    error: 'La institución no se encuentra registrada',
                }, HttpStatus.NO_CONTENT);
            }
        }else{
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'No tiene los permisos suficientes',
            }, HttpStatus.FORBIDDEN);
        }
    }
}
