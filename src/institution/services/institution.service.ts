import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { InstitutionDto, UpdateInstitutionDto } from '../dto/institution.dto';
import { InstitutionEntity } from '../entities/institution.entity';

@Injectable()
export class InstitutionService {
    constructor(
        @InjectRepository(InstitutionEntity)
        private readonly institutionRepository:Repository<InstitutionEntity>
    ){}
    
    async getAllWithInstitution(){
        return await this.institutionRepository.find();
    }

    async getOne(id:number){
        return await this.institutionRepository.findOne(id);
    } 

    async getOneByName(nameInstitution:string){
        return await this.institutionRepository.findOne({nameInstitution});
    }

    async create(data:InstitutionDto){
        const newInstitution = new InstitutionEntity();
        newInstitution.nameInstitution = data.nameInstitution;
        newInstitution.descriptionInstitution = data.descriptionInstitution;
        newInstitution.abbreviationInstitution = data.abbreviationInstitution;
        return await this.institutionRepository.save(newInstitution)
    }
    
    async update(data: UpdateInstitutionDto){
        const institution = await this.institutionRepository.findOne(data.idInstitution);
        if(institution){
            institution.nameInstitution = data.nameInstitution;
            institution.descriptionInstitution = data.descriptionInstitution;
            institution.abbreviationInstitution = data.abbreviationInstitution;
        }
        return await this.institutionRepository.save(institution);
    }

    async delete(idInstitution: number){
        return await this.institutionRepository.delete(idInstitution);
    }
}
