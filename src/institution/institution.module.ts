import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstitutionEntity } from './entities/institution.entity';
import { InstitutionController } from './controllers/institution.controller';
import { InstitutionService } from './services/institution.service';
import { UserEntity } from 'src/user/entities/user.entity';
import { AuditHandlerService } from 'src/audit-handler/services/audit-handler.service';
import { LogExceptionHandlerService } from 'src/log-exception-handler/services/log-exception-handler.service';
import { AuditHandlerEntity } from 'src/audit-handler/entities/audit-handler.entity';
import { LogExceptionHandlerEntity } from 'src/log-exception-handler/entities/log-exception-handler.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            InstitutionEntity,
            UserEntity,
            AuditHandlerEntity,
            LogExceptionHandlerEntity
        ]),
    ],
    providers: [InstitutionService,AuditHandlerService,LogExceptionHandlerService],
    controllers: [InstitutionController],
    exports: [InstitutionService]
})
export class InstitutionModule {}
