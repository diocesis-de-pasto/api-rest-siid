import { UserEntity } from "src/user/entities/user.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('institutions')
export class InstitutionEntity {
    @PrimaryGeneratedColumn()
    idInstitution:number;

    @Column({unique:true})
    nameInstitution: string;

    @Column({nullable:true})
    abbreviationInstitution:string;

    @Column({nullable:true})
    descriptionInstitution:string;

    @OneToMany(() => UserEntity, user => user.institution)
    user: UserEntity[];
}
