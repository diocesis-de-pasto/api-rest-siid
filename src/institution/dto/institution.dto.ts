import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class InstitutionDto {
    @IsNumber()
    @ApiProperty()
    idInstitution:number;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    nameInstitution: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    abbreviationInstitution:string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    descriptionInstitution:string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    user: number;
}

export class UpdateInstitutionDto extends PartialType(InstitutionDto) {}
