const nodemailer = require('nodemailer');
import { ConfigService } from '@nestjs/config';
import { textEmailNewUser } from './templates/new-user';
import { textEmailRestorePassword } from './templates/restore-password';

export const SendEmail = (type,jsonData) => {    
    var textEmail;
    var emailSend;
    switch(type) {
        case 'AccountCreation':
            var {email,password} = jsonData;
            emailSend = email;
            textEmail = textEmailNewUser(email,password);
            break;
        case 'RestorePassword':
            var {email,token} = jsonData;
            emailSend = email;
            textEmail = textEmailRestorePassword(token);
            break;
    }
    const config = new ConfigService();

    const mailOptions = {
        from: config.get<string>('EMAIL_FROM'),
        to: emailSend,
        subject: "SIID - Diócesis de Pasto",
        html: textEmail
    };
    const transporter = nodemailer.createTransport({
        host: 'email-smtp.us-east-1.amazonaws.com',
        port: 587,
        secure: false,
        auth: {
            user: config.get<string>('USER_EMAIL'),
            pass: config.get<string>('PASS_USER_EMAIL')
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    
    transporter.sendMail(mailOptions)
        .then(res=>{
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        })
}