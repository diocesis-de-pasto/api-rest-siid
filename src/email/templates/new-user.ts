import { headboard, end } from "./all-email";

export const textEmailNewUser = (email,password) => { 
    return `${headboard}
        <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">
            Creación de cuenta</h1>
        <p
            style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">
            Se ha creado una cuenta en la plataforma SIID de la Diocesis de Pasto
            <br>
            Usuario: ${email}
            <br>
            Contraseña: ${password}
        
        </p>
        <p
            style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">
            <a href="${process.env.PATHDDP}"
                style="color:#ee4c50;text-decoration:underline;">Ingresar para actualizar sus datos</a></p>
        ${end}`;
}