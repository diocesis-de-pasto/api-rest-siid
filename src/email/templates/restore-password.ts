import { headboard, end } from "./all-email";

export const textEmailRestorePassword = (token) => {    
    return `${headboard}
        <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">
            Restauración de contraseña</h1>
        <p
            style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">
            Se ha solicitado cambiar la contraseña, si ha sido usted haga 
            click en el siguiente enlace para restaurarla de lo contrario ignore este correo:
        </p>
        <p
            style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">
            <a href="${process.env.PATHDDP}recovery-password/${token}"
                style="color:#ee4c50;text-decoration:underline;">Recuperar contraseña
            </a>
        </p>
        ${end}`;
}