export const headboard = `<!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="x-apple-disable-message-reformatting">
        <title>Creación de cuenta</title>
        <style>
            table,
            td,
            div,
            h1,
            p {
                font-family: Arial, sans-serif;
            }
        </style>
    </head>
    
    <body style="margin:0;padding:0;">
        <table role="presentation"
            style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
            <tr>
                <td align="center" style="padding:0;">
                    <table role="presentation"
                        style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                        <tr>
                            <td align="center" style="padding:20px 0 20px 0;background:linear-gradient(175deg, #b58433 0, #ae7926 50%, #a76e19 100%)">
                                <img src="https://siid.diocesisdepasto.org/assets/img/logo-diocesis.png" alt="Diócesis de Pasto" height="200px"
                                    style="height:200px;display:block;" />
                                <h2 style="color:white;">SISTEMA INTEGRADO DE INFORMACIÓN DIOCESANA</h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:36px 30px 42px 30px;">
                                <table role="presentation"
                                    style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                                    <tr>
                                        <td style="padding:0 0 36px 0;color:#153643;">`;

export const end = `</td>
    </tr>
    <tr>
    <td style="padding:30px;background:#70bbd9;">
    <table role="presentation"
        style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
        <tr>
            <td style="padding:0;width:50%;" align="left">
                <p
                    style="margin:0;font-size:14px;line-height:16px;font-family:Arial,sans-serif;color:#ffffff;">
                    &reg; Diocesis de Pasto, SIID 2021<br /><a href="https://diocesisdepasto.org/"
                        style="color:#ffffff;text-decoration:underline;">Dejar de recibir correos</a>
                </p>
            </td>
            <td style="padding:0;width:50%;" align="right">
                <table role="presentation"
                    style="border-collapse:collapse;border:0;border-spacing:0;">
                    <tr>
                        <td style="padding:0 0 0 10px;width:38px;">
                            <a href="https://twitter.com/diocesispasto?lang=es" style="color:#ffffff;"><img
                                    src="https://assets.codepen.io/210284/tw_1.png"
                                    alt="Twitter" width="38"
                                    style="height:auto;display:block;border:0;" /></a>
                        </td>
                        <td style="padding:0 0 0 10px;width:38px;">
                            <a href="https://www.facebook.com/DiocesisDePastoOficial/" style="color:#ffffff;"><img
                                    src="https://assets.codepen.io/210284/fb_1.png"
                                    alt="Facebook" width="38"
                                    style="height:auto;display:block;border:0;" /></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </body>

    </html>`;