import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditHandlerEntity } from './entities/audit-handler.entity';
import { AuditHandlerService } from './services/audit-handler.service';

@Module({
    imports: [TypeOrmModule.forFeature([AuditHandlerEntity])],
    providers: [AuditHandlerService],
    exports: [AuditHandlerService]
})
export class AuditHandlerModule { }
