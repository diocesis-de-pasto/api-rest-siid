import {
    IsString,
    IsNumber,
    IsNotEmpty,
    IsPositive,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AuditHandlerDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly ipClient: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly browserData: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly endpoint: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly actionPerformed: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly entityType: string;

    @IsString()
    @ApiProperty()
    readonly previousEntity: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly newEntity: string;

    @IsNumber()
    @IsPositive()
    @IsNotEmpty()
    @ApiProperty()
    readonly userId: number;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly userEmail: string;
}