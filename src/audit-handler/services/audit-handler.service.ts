import { Injectable, Req } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuditHandlerDto } from '../dto/audit-handler.dto';
import { AuditHandlerEntity } from '../entities/audit-handler.entity';

@Injectable()
export class AuditHandlerService {
    constructor(
        @InjectRepository(AuditHandlerEntity)
        private readonly auditRepository:Repository<AuditHandlerEntity>,
    ) { }

    async create(@Req() req, entity: string, previousEntity: string, newEntity: string) {
        var ip = (req.headers['x-forwarded-for'] || '').split(',').pop().trim() ||
            req.socket.remoteAddress
        var action = 'create';
        if (req.method === 'PUT') {
            action = 'update';
        } else if (req.method === 'DELETE') {
            action = 'delete';
        }

        const data:AuditHandlerDto = {
            ipClient: ip,
            browserData: req.headers['user-agent'],
            endpoint: req.url,
            actionPerformed: action,
            entityType: entity,
            previousEntity,
            newEntity,
            userId: req.user.idUser,
            userEmail: req.user.email
        };
        await this.auditRepository.save(data);
    }
}