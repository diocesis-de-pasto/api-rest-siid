import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('audit')
export class AuditHandlerEntity {
    @PrimaryGeneratedColumn()
    idAudit: number;

    @Column()
    ipClient: string;

    @Column()
    browserData: string;

    @Column()
    endpoint: string;

    @Column()
    actionPerformed: string;

    @Column()
    entityType: string;

    @Column({ nullable: true })
    previousEntity: string;

    @Column()
    newEntity: string;

    @Column()
    userId: number;

    @Column()
    userEmail: string;

    @CreateDateColumn({
        type: 'timestamptz',
        default: () => 'CURRENT_TIMESTAMP'
    })
    date: Date;
}
